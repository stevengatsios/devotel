package di.uoa.memory;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * 
 * Data Class to hold Statistical Reports of a Client from last report until now
 * 
 * @author Kostas
 *
 */
public class StatisticalReports {
	private S_MPSM s_mpsm;
	private String client;
	private Timestamp timestamp = null;
	
	/**
	 * Constructor
	 * 
	 * @param s_mpsm
	 */
	public StatisticalReports(S_MPSM s_mpsm) {
		this.s_mpsm = s_mpsm;
		timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime()
				.getTime());
	}
	
	/**
	 * Constructor
	 * 
	 * @param s_mpsm
	 * @param client
	 */
	public StatisticalReports(S_MPSM s_mpsm, String client, Timestamp timestamp) {
		this.s_mpsm = s_mpsm;
		this.client = client;
		this.timestamp = timestamp;
	}

	/**
	 * Needed for deserialization.
	 */
	public StatisticalReports() {
		
	}

	public S_MPSM getS_MPSM() {
		return s_mpsm;
	}

	/**
	 * Should be used via constructor to create id and time-stamp
	 * 
	 * @deprecated
	 * @param s_mpsm
	 */
	public void setS_MPSM(S_MPSM s_mpsm) {
		this.s_mpsm = s_mpsm;
	}
		
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
