<!DOCTYPE html>
<%@page import="di.uoa.server.database.bases.StatisticsBase"%>
<%@page import="java.sql.ResultSet"%>
<html>
<head>
<%  %>
<%@include file="includes/header.jsp"%>
<title>Dashboard</title>
</head>
<body>
	<%@include file="includes/navbar.jsp"%>
	<div class="container" style="width: auto;">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Client</th>
					<th>Interface</th>
					<th>Pattern</th>
					<th>Frequency</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody>
				<%
					ResultSet result = StatisticsBase.getHistory();
					while(result.next()) {
				%>
				<tr>
					<td><%=result.getString("Clients_idClient")%></td>
					<td><%=result.getString("interface") + " - "+ result.getString("interfaceIP")%></td>
					<td><%=result.getString("Malicious_pattern")%></td>
					<td><%=result.getInt("frequency")%></td>
					<td><%=result.getString("timestamp")%></td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
	<%@include file="includes/scripts.jsp"%>
</body>
</html>