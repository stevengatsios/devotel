package di.uoa.server.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import di.uoa.memory.StatisticalReports;
import di.uoa.memory.S_MPSM.Register;
import di.uoa.server.database.DAO;
import di.uoa.server.database.bases.ClientsBase;
import di.uoa.server.database.bases.StatisticsBase;
import di.uoa.server.memory.Node;

/**
 * Web Service Class for Reports
 * 
 * @author Kostas
 *
 */
@Path("/reports")
public class Reports {

	/**
	 * Interface which sends Statistical Reports to the Server
	 * 
	 * @param nodeID
	 * @param statistics
	 */
	@POST
	@Path("/statistics")
	@Consumes(MediaType.APPLICATION_JSON)
	public void maliciousPatternsStatisticalReport(
			@QueryParam("id") String nodeID, StatisticalReports statistics) {

		for (Register record : statistics.getS_MPSM()) {

			DAO.insertStatistics(record.getInterfaceName(),
					record.getInterfaceIP(), record.getFrequency(), nodeID,
					record.getMaliciousPattern(), statistics.getTimestamp());

		}

	}
	
	/**
	 * Send a request to the Server and retrieve Statistics of Android user's network
	 * with {@param username} and {@param password}
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@GET
	@Path("/android/statistics")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StatisticalReports> retrieveStatistics(
			@QueryParam("username") String username,
			@QueryParam("password") String password) {
		List<StatisticalReports> reports = new ArrayList<StatisticalReports>();
		for(Node node : ClientsBase.getClients()) {
			reports.add(StatisticsBase.getStatisticsByID(node.getUserID()));
		}
		return reports;
	}
}
