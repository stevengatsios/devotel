package di.uoa.user.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Utility database Class to Create and Upgrade Database
 * see {@link SQLiteOpenHelper}
 * 
 * @author Kostis
 *
 */
public class MySqliteHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "anaptuxh.db";
	// tables
	public static final String TABLE_NODES = "Nodes"; // Devices
	public static final String TABLE_MALICIOUS = "Malicious";
	public static final String TABLE_STATS = "Statistics";
	// primary key of all
	public static final String COLUMN_ID = "_id";
	// Nodes columns
	public static final String COLUMN_NODE = "node";
	// Malicious columns
	public static final String COLUMN_PATTERN = "pattern";
	// Statistics columns
	public static final String COLUMN_N_NODE = "Nodes_node";
	public static final String COLUMN_INTERFACE = "interface";
	public static final String COLUMN_INTERFACEIP = "interfaceIP";
	public static final String COLUMN_M_PATTERN = "Malicious_pattern";
	public static final String COLUMN_FREQ = "frequency";
	public static final int DATABASE_VERSION = 4;

	// Database creation sql statements
	private static final String NODES_CREATE = "create table "
			+ TABLE_NODES + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_NODE
			+ " text not null);";
	
	private static final String MALICIOUS_CREATE = "create table "
			+ TABLE_MALICIOUS + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_PATTERN
			+ " text not null);";
	
	private static final String STATS_CREATE = "create table "
			+ TABLE_STATS + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_N_NODE + " text not null,"
			+ COLUMN_INTERFACE + " text not null," 
			+ COLUMN_INTERFACEIP + " text not null,"
			+ COLUMN_M_PATTERN + " text not null,"
			+ COLUMN_FREQ + " long);";

	public MySqliteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(NODES_CREATE);
		database.execSQL(MALICIOUS_CREATE);
		database.execSQL(STATS_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySqliteHelper.class.getName(), "Upgding database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NODES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MALICIOUS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATS);
		onCreate(db);
	}

}
