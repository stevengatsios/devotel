package di.uoa.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import di.uoa.settings.Settings;

/**
 * Package class for accessing the database.
 * 
 * @author Steve, Kostis
 *
 */
public class DAO {

	private String url;
	private String user;
	private String password;
	private static DAO instance = null;
	private static Connection connect = null;
	private static PreparedStatement preparedStatement = null;
	private static Statement statement = null;
	private static ResultSet resultSet = null;
	
	/**
	 * Executes an Update like Insert, Delete or Update
	 * 
	 * @param update
	 * @return
	 */
	synchronized public static boolean executeUpdate(String update) {
		try {
			preparedStatement = DAO.getConnection().prepareStatement(update);
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	
	/**
	 * Inserts a record to Statistics Table
	 * 
	 * @param interfaceName
	 * @param interfaceIP
	 * @param freq
	 * @param clientID
	 * @param pattern
	 * @param registerID
	 * @param stamp
	 * @return
	 */
	synchronized public static boolean insertStatistics(String interfaceName,
			String interfaceIP, long freq, String clientID, String pattern,
			Timestamp stamp) {
		try {
			String sqlInsertStatement = "INSERT INTO Statistics "
					+ "(interface, interfaceIP, frequency, Clients_idClient, "
					+ "Malicious_pattern, timestamp) VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = DAO.getConnection()
					.prepareStatement(sqlInsertStatement);
			preparedStatement.setString(1, interfaceName);
			preparedStatement.setString(2, interfaceIP);
			preparedStatement.setLong(3, freq);
			preparedStatement.setString(4, clientID);
			preparedStatement.setString(5, pattern);
			preparedStatement.setTimestamp(6, stamp);

			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	
	/**
	 * Executes a query to database
	 * 
	 * @param query
	 * @return ResultSet
	 */
	synchronized public static ResultSet executeQuery(String query) {
		try {
			statement = DAO.getConnection().createStatement();
			resultSet = statement.executeQuery(query);
			return resultSet;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			System.err.println(e.getSQLState());
			return null;
		}
	}
	
	/**
	 * Get or create a connection
	 * 
	 * @return Connection
	 */
	private static Connection getConnection() {
		DAO db = DAO.getInstance();
		if (connect == null) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e1) {
				throw new RuntimeException(e1);
			}
			try {
				connect = DriverManager.getConnection(db.url, db.user,
						db.password);
			} catch (SQLException e) {
				System.err.println(e.getMessage());
				return null;
			}
		}
		return connect;
	}
	
	/**
	 * Close current Statement
	 */
	synchronized public static void closeStatement() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Close the connection with database
	 */
	public static void disconnect() {
		if (connect != null) {
			try {
				connect.close();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	synchronized private static DAO getInstance() {
		if (instance == null) {
			instance = new DAO();
		}
		return instance;
	}
	
	/**
	 * Constructor
	 */
	private DAO() {
		// "jdbc:mysql://"
		url = "jdbc:mysql://" + Settings.getValue("db.host") + "/"
				+ Settings.getValue("db.schema") + "?autoReconnect=true";
		user = Settings.getValue("db.user");
		password = Settings.getValue("db.password");
	}
}
