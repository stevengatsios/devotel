package di.uoa.client.register;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

/**
 * Create a unique User ID
 * 
 * @author Kostis
 *
 */

public class UserID {
	private static UserID instance = null;

	/**
	 * if file ".id" exists read id else create this file and write a new
	 * id,created by {@link UUID} DO IT ONCE
	 * 
	 * @return {@link UserID}
	 */
	public static synchronized UserID getInstance() {
		if (UserID.instance == null) {
			UserID.instance = new UserID();
		}
		return UserID.instance;
	}

	private String uid;

	private UserID() {
		BufferedReader br;
		try {
			File file = new File(".id");
			br = new BufferedReader(new FileReader(file));
			try {
				this.uid = br.readLine();
				br.close();
			} catch (IOException e) {
				uid = null;
			}
		} catch (FileNotFoundException e) {
			try {
				File file = new File(".id");
				BufferedWriter output = new BufferedWriter(new FileWriter(file));
				this.uid = UUID.randomUUID().toString();
				output.write(uid);
				output.close();
			} catch (IOException e1) {
				this.uid = null;
			}
		}
	}


	/**
	 * getter of uid
	 * 
	 * @return uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * setter of uid
	 * 
	 * @param userid
	 */
	public void setUid(String userid) {
		this.uid = userid;
	}

}
