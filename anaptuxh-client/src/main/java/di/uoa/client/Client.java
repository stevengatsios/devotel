package di.uoa.client;

import java.util.Scanner;

/**
 * Client
 * 
 * @author Steve
 *
 */
public class Client {

	/**
	 * variables for the Client
	 */
	static ClientFacade client;
	

	/**
	 * Parses a line from the terminal and executes the corresponding command
	 * 
	 * @param arg
	 * @param client
	 * @return
	 */
	private static boolean cli_parse(String arg, ClientFacade client) {
		if ("exit".equals(arg)) {
			return true;
		}
		else if("print mpsm".equals(arg)) {
			System.out.println(client.mpsm.toString());
		}
		else if("print smpsm".equals(arg)) {
			System.out.println(client.s_mpsm.toString());
		}
		return false;
	}

	/**
	 * Main thread of the application
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		ClientFacade client = ClientFacade.getInstance();
		Scanner sc = new Scanner(System.in);

		System.out.println("Starting");
		client.register();
		client.serverSync();
		client.startSniffing();
		while (true) {
			if (sc.hasNext()) {
				String arg = sc.nextLine();
				if (cli_parse(arg, client)) {
					break;
				}
			}
		}
		sc.close();
		client.terminate();
	}
}
