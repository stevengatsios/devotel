package di.uoa.client.thread;

import java.util.TimerTask;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.jackson.JacksonFeature;

import di.uoa.client.register.UserID;
import di.uoa.client.settings.ServerProperties;
import di.uoa.memory.MPSM;
import di.uoa.memory.MaliciousPatterns;
import di.uoa.settings.Settings;

/**
 * Updates the instant of mpsm from the server.
 * 
 * @author Steve
 *
 */
public class ServerCommunication extends TimerTask {

	public static int INTERVAL;
	private MPSM mpsm = null;
	private Client client;

	public ServerCommunication(MPSM mpsm) {
		this.mpsm = mpsm;
		INTERVAL = Integer.parseInt(Settings.getValue("timer.update"));
		//ClientConfig conf = new DefaultClientConfig(JacksonFeature.class);
		client = ClientBuilder.newClient().register(JacksonFeature.class);
	}

	@Override
	public void run() {
		updateMPSM();
	}

	/**
	 * Send request for new malicious patterns to the Server.
	 */
	private MaliciousPatterns sendRequest() {
		WebTarget webResource = client.target(
				ServerProperties.getRestPath() + "/requests/malicious")
				.queryParam("id", UserID.getInstance().getUid());
		return webResource.request(MediaType.TEXT_PLAIN)
				.accept(MediaType.APPLICATION_JSON)
				.get(MaliciousPatterns.class);
	}
	
	/**
	 * update MPSM with new Malicious Patterns
	 */
	private void updateMPSM() {
		MaliciousPatterns malicious = sendRequest();
		mpsm.addAll(malicious.getAdded());
		mpsm.removeAll(malicious.getDeleted());
	}

}
