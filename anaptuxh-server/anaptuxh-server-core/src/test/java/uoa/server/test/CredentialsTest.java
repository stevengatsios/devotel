package uoa.server.test;

import static org.junit.Assert.*;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import di.uoa.server.resources.Credentials;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CredentialsTest extends JerseyTest {

	
	@Override
	protected Application configure() {
		return new ResourceConfig(Credentials.class);
	}
	
	@Test
	public void AtestInsert() {
		Entity<String> body = Entity.entity("TEST_ID", MediaType.TEXT_PLAIN);
		Response response = target("credentials").path("register").request().post(body);
		String resp = response.readEntity(String.class);
		assertEquals("true", resp);
	}
	
	@Test
	public void BtestDelete() {
		Entity<String> body = Entity.entity("TEST_ID", MediaType.TEXT_PLAIN);
		Response response = target("credentials").path("unregister").request().post(body);
		String resp = response.readEntity(String.class);
		assertEquals("true", resp);
	}

}
