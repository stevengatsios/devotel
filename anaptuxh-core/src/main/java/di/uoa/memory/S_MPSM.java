package di.uoa.memory;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 
 * @authors Kostis, Steve, Kostas
 *
 */
public class S_MPSM extends ArrayList<S_MPSM.Register> {

	public static class Register {
		Long frequency;
		String interfaceIP;
		String interfaceName;
		String maliciousPattern;

		public Register(String interfaceName, String interfaceIP,
				String maliciousPattern, Long frequency) {
			super();
			this.interfaceName = interfaceName;
			this.interfaceIP = interfaceIP;
			this.maliciousPattern = maliciousPattern;
			this.frequency = frequency;
		}

		/**
		 * Needed for deserialization.
		 */
		public Register() {
		}

		public Long getFrequency() {
			return frequency;
		}

		public String getInterfaceIP() {
			return interfaceIP;
		}

		public String getInterfaceName() {
			return interfaceName;
		}

		public String getMaliciousPattern() {
			return maliciousPattern;
		}

		public void setFrequency(Long frequency) {
			this.frequency = frequency;
		}

		public void setInterfaceIP(String interfaceIP) {
			this.interfaceIP = interfaceIP;
		}

		public void setInterfaceName(String interfaceName) {
			this.interfaceName = interfaceName;
		}

		public void setMaliciousPattern(String maliciousPattern) {
			this.maliciousPattern = maliciousPattern;
		}

		@Override
		public String toString() {
			return this.interfaceName + " # " + this.interfaceIP + " # "
					+ this.maliciousPattern + " # " + this.frequency;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ReentrantReadWriteLock lock;
	
	/**
	 * Constructor
	 */
	public S_MPSM() {
		super();
		lock = new ReentrantReadWriteLock();
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("		S_MPSM\n");
		ret.append("IntefaceName # InterfaceIP # Malicious # Frequency\n");
		lock.readLock().lock();
		if (!this.isEmpty()) {
			for (Register x : this) {
				ret.append(x.toString() + "\n");
			}
		}
		lock.readLock().unlock();
		return ret.toString().trim();
	}

	/**
	 * Creates or Updates an S_MPSM register which holds the frequency of
	 * appearances of a malicious IP or of a malicious pattern (malicious) for a
	 * specific device (inter)
	 * 
	 * @param interfaceName
	 *            The name of the interface.
	 * @param interfaceIp
	 *            the Ip of the interface.
	 * @param malicious
	 *            the malicious pattern.
	 * @param freq
	 *            the frequency to be added.
	 * 
	 * @return nothing
	 */
	public void updateRegister(String interfaceName, String interfaceIp,
			String malicious, long freq) {
		boolean newReg;
		newReg = true;
		String name = interfaceName;
		String IP = interfaceIp;
		lock.writeLock().lock();
		for (Register reg : this) {
			if (name.equals(reg.getInterfaceName())) {
				if (malicious.equals(reg.getMaliciousPattern())) {
					reg.setFrequency(reg.getFrequency() + freq);
					newReg = false;
					break;
				}
			}
		}
		if (newReg) {
			this.add(new Register(name, IP, malicious, (long) freq));
		}
		lock.writeLock().unlock();
	}

}
