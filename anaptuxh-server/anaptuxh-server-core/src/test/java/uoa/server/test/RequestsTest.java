package uoa.server.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import di.uoa.memory.MaliciousPatterns;
import di.uoa.server.database.DAO;
import di.uoa.server.resources.Patterns;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class RequestsTest extends JerseyTest {

	final static String PATTERN = "test";
	final static String IP = "100.100.100.100";
	final static String UUID = "TEST_ID";
	
	@Override
    protected Application configure() {
        return new ResourceConfig(Patterns.class);
    }
	
	@BeforeClass
	public static void createPattern() {
		DAO.executeUpdate("INSERT INTO Malicious VALUES ('" + PATTERN +"',true)");
		DAO.executeUpdate("INSERT INTO Malicious VALUES ('"+ IP +"',true)");
	}
	
	@Test
    public void AtestInitialRequest() {
        final MaliciousPatterns responseMsg = target("requests").path("malicious").queryParam("id", UUID).request().get(MaliciousPatterns.class);
        System.out.println(responseMsg.toString());
        assertNotNull(responseMsg);
        assertEquals("Should have been populated [patterns]",true,responseMsg.getAdded().getMaliciousPatterns().size()>0);
        assertEquals("Should have been populated [ips]",true,responseMsg.getAdded().getMaliciousIPs().size()>0);
    }
	
	@Test
    public void BtestSubsequentRequest() {
        final MaliciousPatterns responseMsg = target("requests").path("malicious").queryParam("id", UUID).request().get(MaliciousPatterns.class);
        System.out.println(responseMsg.toString());
        assertNotNull(responseMsg);
        assertEquals("Should have been updated [patterns]",0,responseMsg.getAdded().getMaliciousPatterns().size());
        assertEquals("Should have been updated [ips]",0,responseMsg.getAdded().getMaliciousIPs().size());
    }
	
	@Test
	public void CremoveIP() {
		assertEquals(true, DAO.executeUpdate("DELETE FROM Malicious where pattern = '"+ IP +"'"));
	}
	
	
	@Test
    public void DtestDeletionRequest() {
        final MaliciousPatterns responseMsg = target("requests").path("malicious").queryParam("id", UUID).request().get(MaliciousPatterns.class);
        assertNotNull(responseMsg);
        assertEquals("Should have been deleted [ips]",1,responseMsg.getDeleted().getMaliciousIPs().size());
    }
	
	@AfterClass
	public static void deletePattern() {
		DAO.executeUpdate("DELETE FROM Malicious WHERE pattern ='" + PATTERN +"'");
	}
}
