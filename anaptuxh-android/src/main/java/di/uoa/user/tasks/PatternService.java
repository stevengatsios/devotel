package di.uoa.user.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import di.uoa.user.activities.fragments.AdminFragment;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.ServerConnector;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class PatternService extends IntentService {

	private static final int WAIT = 60 * 1000; // every 1 minute
	private static final String PATH_INSERT = "admin/tools/insert";
	private static final String PATH_DELETE = "admin/tools/remove";
	/**
	 * Is the pattern added or the node deleted.
	 */
	public static final String BROADCAST_MESSAGE = "PatternService";
	/**
	 * Can be PARAM_INSERT_PATTERN or PARAM_DELETE_NODE
	 */
	public static final String BROADCAST_ACTION = "ACTION";
	public static final String PARAM_INSERT_PATTERN = "pattern";
	public static final String PARAM_DELETE_NODE = "delete";

	public PatternService() {
		super("Pattern Service");
	}

	@Override
	protected void onHandleIntent(Intent arg0) {
		String pattern = arg0.getStringExtra(PARAM_INSERT_PATTERN);
		String nodeDelete = arg0.getStringExtra(PARAM_DELETE_NODE);
		boolean sent = false;
		while (!sent) {
			try {
				if(pattern != null) {
					insertPattern(pattern);
				} else {
					deleteNode(nodeDelete);
				}
				//Gia na vrisketai edw shmainei oti egine me epituxia
				Intent broadcastIntent = new Intent(AdminFragment.ServerUpdateReceiver.BROADCAST_FILTER);
				broadcastIntent.putExtra(BROADCAST_ACTION, (pattern == null) ? PARAM_DELETE_NODE : PARAM_INSERT_PATTERN);
				broadcastIntent.putExtra(BROADCAST_MESSAGE, (pattern == null) ? nodeDelete : pattern);
				LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
				sent = true;
				break;
			} catch (AppException e) {
				Log.i(this.getClass().toString(),"Tried but failed, lets try again " + pattern + " "+ e.getMessage());
			}
			try {
				Thread.sleep(WAIT); //TODO SystemClock.sleep() den 3erw an 8elei permissions
			} catch (InterruptedException e) {
				Log.e(this.getClass().toString(),"Something bad happened " + e.getMessage());
				break;
			}
		}
	}
	
	/**
	 * Send new pattern to the Server
	 * 
	 * @param pattern
	 * @throws AppException
	 */
	private void insertPattern(String pattern) throws AppException{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", Account.getUsername()));
		params.add(new BasicNameValuePair("pattern", pattern));
		ServerConnector.getInstance().doPOST(PATH_INSERT, params, Account.getPassword(), null);
	}
	
	/**
	 * Send request to the Server delete a node from the system
	 * 
	 * @param node
	 * @throws AppException
	 */
	private void deleteNode(String node) throws AppException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", Account.getUsername()));
		params.add(new BasicNameValuePair("password", Account.getPassword()));
		ServerConnector.getInstance().doPOST(PATH_DELETE, params, node, null);
	}

}
