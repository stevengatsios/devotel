package di.uoa.server.gui.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import di.uoa.memory.MPSM;
import di.uoa.server.database.DAO;
import di.uoa.server.database.bases.MaliciousBase;

/**
 * 
 * @author Kostis
 *
 */
public class IpPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JList<String> list;
	DefaultListModel<String> model;

	/**
	 * Constructor - Create a {@link JPanel} that displays IPs and has the
	 * options Add IP and Remove IP.
	 */
	public IpPanel() {
		setLayout(new BorderLayout());
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		list.setVisibleRowCount(15);
		JScrollPane pane = new JScrollPane(list);
		JButton addButton = new JButton("Add IP");
		JButton removeButton = new JButton("Remove IP");
		for (String ip : MaliciousBase.getMaliciousIPs(true)) {
			model.addElement(ip);
		}

		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ip = null;
				ip = JOptionPane.showInputDialog(list, "Enter a malicious IP:");
				if (ip != null) {
					if (MPSM.isIp(ip)) {
						if (!model.contains(ip)
								&& DAO.executeUpdate("INSERT INTO Malicious "
										+ "VALUES('" + ip + "', true)")) {
							model.addElement(ip);
						} else {
							JOptionPane.showMessageDialog(list, "This IP "
									+ "already exists");
						}
					} else {
						JOptionPane.showMessageDialog(list, "This IP "
								+ "is not valid");
					}
				}
			}
		});

		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (model.getSize() > 0) {
					String remove = list.getSelectedValue();
					if (remove != null) {
						int confirm = JOptionPane.showConfirmDialog(list,
								"Are you sure you wish to move '" + remove
										+ "' to the recycle bin ?");
						if (remove != null && confirm == 0) {
							model.removeElement(remove);
							DAO.executeUpdate("UPDATE Malicious SET valid=false"
									+ " WHERE pattern='" + remove + "'");
						}
					}
				}
			}
		});

		add(pane, BorderLayout.NORTH);
		add(addButton, BorderLayout.WEST);
		add(removeButton, BorderLayout.EAST);
	}

}
