package di.uoa.server.memory;

import di.uoa.memory.MPSM;

/**
 * Data Class for each Client
 * 
 * @author Kostis
 *
 */
public class Node {

	private String userID;
	private boolean online;
	private MPSM mpsm;

	public Node(String userID, boolean online) {
		this.userID = userID;
		this.mpsm = new MPSM();
		this.online = online;
	}
	
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public MPSM getMpsm() {
		return mpsm;
	}

	public void setMpsm(MPSM mpsm) {
		this.mpsm = mpsm;
	}	

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof Node)) {
			return false;
		}
		Node obj = (Node) other;
		return this.userID.equals(obj.userID);
	}

}
