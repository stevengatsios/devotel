package di.uoa.user.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import di.uoa.user.R;
import di.uoa.user.db.bases.NodeBase;
import di.uoa.user.db.bases.StatisticBase;
import di.uoa.user.db.tables.Statistic;
import di.uoa.user.utils.Account;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Adapter to create expandable list of UUID to Statistics of Malicious-Frequency
 * 
 * @author Kostas
 *
 */
public class ExpandableMaliciousAdapter extends AbstractExpandableAdapter {

	private HashMap<String, List<Statistic>> _listDataChild;

	public ExpandableMaliciousAdapter(Context context, boolean myDevice) {
		super(context);
		if(myDevice) {
			this._listDataHeader = new ArrayList<String>(Account.getAvailableNodes());
		} else {
			NodeBase nb = new NodeBase(context);
			nb.open();
			this._listDataHeader = nb.getAllNodes();
			nb.close();
		}
		StatisticBase sb = new StatisticBase(context);
		sb.open();
		this._listDataChild = sb.getAllStatisticsAsMap(this._listDataHeader);
		sb.close();
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final Statistic childStatistic = (Statistic) getChild(groupPosition, childPosition);
		final String childText = childStatistic.getMalicious();
		final long childFreq = childStatistic.getFrequency();

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.exp_malicious_layout, parent, false);
		}

		TextView maliciousListChild = (TextView) convertView
				.findViewById(R.id.exp_malicious);
		TextView freqListChild = (TextView) convertView
				.findViewById(R.id.exp_malicious_freq);

		maliciousListChild.setText(childText);
		freqListChild.setText(""+childFreq);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}
}