package di.uoa.user.db.tables;

/**
 * Data Class to represents Nodes Table of database
 * 
 * @author Kostis
 *
 */
public class Node {
	
	private String node;
	private long id;
	
	public Node(String node, long id) {
		this.setNode(node);
		this.setId(id);
	}
	
	public Node() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long l) {
		this.id = l;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}
}
