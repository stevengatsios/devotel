package uoa.server.test;

import static org.junit.Assert.assertEquals;



import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import di.uoa.server.database.DAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JdbcTest {
	/*
	 * DO NOT assign a statistics report to 'TEST PATTERN' or else it won't be deleted. 
	 */
	
	
	@Test
	@Ignore
	public void Atestinsert() {
		assertEquals(true,DAO.executeUpdate("insert into Malicious values('TEST PATTERN')"));	
	}
	
	@Test
	@Ignore
	public void Btestdelete(){
		assertEquals(true,DAO.executeUpdate("delete from Malicious where pattern = 'TEST PATTERN'"));
	}	

}
