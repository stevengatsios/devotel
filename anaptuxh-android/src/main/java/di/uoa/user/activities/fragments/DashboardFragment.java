package di.uoa.user.activities.fragments;

import di.uoa.user.R;
import di.uoa.user.activities.fragments.tabs.DashboardTab;
import di.uoa.user.activities.fragments.tabs.ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment that displays Statistics.
 * 
 *
 */
public class DashboardFragment extends Fragment {

	private FragmentTabHost mTabHost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(false); //Not sure yet
		mTabHost = new FragmentTabHost(getActivity());
		mTabHost.setup(getActivity(), getChildFragmentManager(),
				R.id.realtabcontent);
		mTabHost.setup(getActivity(), getChildFragmentManager());
		Bundle myDevicesArgument = new Bundle();
		myDevicesArgument.putBoolean(ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.ARGUMENT, true);
		mTabHost.addTab(
				mTabHost.newTabSpec("my_nodes").setIndicator(getActivity().getString(R.string.my_nodes), null),
				DashboardTab.class, myDevicesArgument);
		Bundle allDevicesArgument = new Bundle();
		allDevicesArgument.putBoolean(ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.ARGUMENT, false);
		mTabHost.addTab(
				mTabHost.newTabSpec("all_nodes").setIndicator(getActivity().getString(R.string.all_nodes), null),
				DashboardTab.class, allDevicesArgument);
		return mTabHost;
	}
	
	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.dashboard, menu);
    }
}
