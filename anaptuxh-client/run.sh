#!/bin/bash
LIBRARY=libpcap.so
LOCATE=`locate $LIBRARY`
if [ $? = 0 ]; then
	echo "Library $LIBRARY Found!"
else
	echo "Library $LIBRARY Not found"
	exit
fi
if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit
fi
if [ -f ./target/Client-*-full.jar ]; then
	java -jar ./target/Client-*-full.jar
fi
