package di.uoa.server.gui;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import di.uoa.settings.Settings;

import java.net.URI;

import javax.ws.rs.ProcessingException;



/**
 * ServerRunner is a thread that starts the server.
 * 
 * @see https://grizzly.java.net/httpserverframework.html
 */
public class ServerRunner extends Thread {
	// Base URI the Grizzly HTTP server will listen on
	private static final String PORT = Settings.getValue("server.port");
	private static final String IP = Settings.getValue("server.ip");
	private static final String PATH = Settings.getValue("server.path");
	private static final String REST = Settings.getValue("server.rest");
	public static final String BASE_URI = "http://" + IP + ":" + PORT + "/" + PATH + "/" + REST + "/";
	final static public String RUNNING_STATUS = "Ο server τρέχει στον σύνδεσμο : " + BASE_URI;
	final static public String INACTIVE_STATUS = "Ο server δεν τρέχει σε αυτό το μηχάνημα";
	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 * 
	 * @return Grizzly HTTP server.
	 * @throws Exception
	 */
	private static HttpServer startServer() {
		// create a resource config that scans for JAX-RS resources and
		// providers
		final ResourceConfig rc = new ResourceConfig()
				.packages("di.uoa.server.resources");
		System.out.println(BASE_URI);
		rc.register(JacksonFeature.class);
		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		HttpServer ret = null;
		try {
			ret = GrizzlyHttpServerFactory.createHttpServer(
					URI.create(BASE_URI), rc);
		} catch (ProcessingException bind) {
			throw new RuntimeException(bind);
		}
		return ret;
	}

	/**
	 * Runs the server as a runnable.
	 */
	public void run() {
		HttpServer server = null;
		server = startServer();
		System.out.println(String.format(
				"Jersey app started with WADL available at "
						+ "%sapplication.wadl\nHit enter to stop it...",
				BASE_URI));
		try {
			synchronized (this) {
				this.wait();
			}
		} catch (InterruptedException e) {
			System.out.println("terminated");
		}
		server.shutdownNow();
	}

}
