package di.uoa.user.activities.fragments;

import di.uoa.user.R;
import di.uoa.user.activities.fragments.tabs.MaliciousTabFragment;
import di.uoa.user.activities.fragments.tabs.NodeTabFragment;
import di.uoa.user.tasks.PatternService;
import di.uoa.user.utils.Accumulator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Fragment for admin tools
 * 
 *
 */
public class AdminFragment extends Fragment {
	private FragmentTabHost mTabHost;
	private boolean createdTab = false;
	private BroadcastReceiver mMessageReceiver;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		mMessageReceiver = new ServerUpdateReceiver();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter(ServerUpdateReceiver.BROADCAST_FILTER));
		mTabHost = new FragmentTabHost(getActivity());
		mTabHost.setup(getActivity(), getChildFragmentManager(),
				R.id.realtabcontent);
		mTabHost.setup(getActivity(), getChildFragmentManager());
		Bundle pattern = new Bundle();
		pattern.putString(MaliciousTabFragment.BUNDLE_OPTION, MaliciousTabFragment.OPTION_PATTERN);
		mTabHost.addTab(
				mTabHost.newTabSpec("tab1").setIndicator(Accumulator.getContext().
											getString(R.string.patterns), null),
				MaliciousTabFragment.class, pattern);
		Bundle ip = new Bundle();
		ip.putString(MaliciousTabFragment.BUNDLE_OPTION, MaliciousTabFragment.OPTION_IP);
		mTabHost.addTab(
				mTabHost.newTabSpec("tab2").setIndicator(Accumulator.getContext().
						getString(R.string.IP), null),
				MaliciousTabFragment.class, ip);
		mTabHost.addTab(
				mTabHost.newTabSpec("tab3").setIndicator(Accumulator.getContext().
						getString(R.string.nodes), null),
				NodeTabFragment.class, null);
		return mTabHost;
	}

	public void onResume() {
		super.onResume();
		if (!createdTab) {
			createdTab = true;
			mTabHost.setup(getActivity(), getActivity()
					.getSupportFragmentManager());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
		mTabHost = null;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		int tab = mTabHost.getCurrentTab();
		if (tab == 0 || tab == 1) {
			inflater.inflate(R.menu.pattern, menu);
		} else {
			inflater.inflate(R.menu.node, menu);
		}
	}
	
	public class ServerUpdateReceiver extends BroadcastReceiver {
		 public static final String BROADCAST_FILTER =   
			      "di.uoa.user.SERVER_UPDATED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getStringExtra(PatternService.BROADCAST_ACTION);
			String data = intent.getStringExtra(PatternService.BROADCAST_MESSAGE);
			String toastText;
			//Format text and insert it int the view adapter
			if(action.equals(PatternService.PARAM_INSERT_PATTERN)) {
				toastText = Accumulator.getContext().getString(R.string.pattern_settings_action);
			} else {
				toastText = Accumulator.getContext().getString(R.string.node_action_setting);
			}
			toastText += " " + data;
			Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();
		}
	}
}
