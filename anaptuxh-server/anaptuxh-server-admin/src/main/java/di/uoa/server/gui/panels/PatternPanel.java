package di.uoa.server.gui.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import di.uoa.memory.MPSM;
import di.uoa.server.database.DAO;
import di.uoa.server.database.bases.MaliciousBase;

/**
 * 
 * @author Kostis
 *
 */
public class PatternPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JList<String> list;
	DefaultListModel<String> model;

	/**
	 * Constructor - Create a {@link JPanel} that displays Patterns and has the
	 * options Add Pattern and Remove Pattern.
	 */
	public PatternPanel() {
		setLayout(new BorderLayout());
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		list.setVisibleRowCount(15);
		JScrollPane pane = new JScrollPane(list);
		JButton addButton = new JButton("Add Pattern");
		JButton removeButton = new JButton("Remove Pattern");
		for (String pattern : MaliciousBase.getMaliciousPatterns(true)) {
			model.addElement(pattern);
		}

		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String pattern = null;
				pattern = JOptionPane.showInputDialog(list,
						"Enter a malicious Pattern:");
				if (pattern != null) {
					if (!MPSM.isIp(pattern)) {
						if (!model.contains(pattern)
								&& DAO.executeUpdate("INSERT INTO Malicious "
										+ "VALUES('" + pattern + "', true)")) {
							model.addElement(pattern);
						} else {
							JOptionPane.showMessageDialog(list, "This Pattern "
									+ "already exists");
						}
					} else {
						JOptionPane.showMessageDialog(list, "This should "
								+ "be added in Malicious IPs");
					}
				}
			}
		});

		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (model.getSize() > 0) {
					String remove = list.getSelectedValue();
					if (remove != null) {
						int confirm = JOptionPane.showConfirmDialog(list,
								"Are you sure you wish to move '" + remove
										+ "' to the recycle bin ?");
						if (confirm == 0) {
							model.removeElement(remove);
							DAO.executeUpdate("UPDATE Malicious SET valid=false"
									+ " WHERE pattern='" + remove + "'");
						}
					}
				}
			}
		});

		add(pane, BorderLayout.NORTH);
		add(addButton, BorderLayout.WEST);
		add(removeButton, BorderLayout.EAST);
	}

}