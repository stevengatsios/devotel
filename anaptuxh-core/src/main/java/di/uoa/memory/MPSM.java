package di.uoa.memory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Data class to hold malicious patterns. Behaves according to read-writer lock
 * problem.
 * 
 * @see ReadWriteLock
 * @author Steve, Kostis
 *
 */
public class MPSM {

	private ReentrantReadWriteLock lock;
	private List<String> maliciousIPs;
	private List<Pattern> maliciousPatterns;
	
	/**
	 * Constructor
	 */
	public MPSM() {
		lock = new ReentrantReadWriteLock();
		maliciousIPs = new ArrayList<String>();
		maliciousPatterns = new ArrayList<Pattern>();
	}
	
	/**
	 * Copy Constructor
	 * 
	 * @param copy
	 */
	public MPSM(MPSM copy) {
		lock = new ReentrantReadWriteLock();
		maliciousIPs = new ArrayList<String>(copy.maliciousIPs);
		maliciousPatterns = new ArrayList<Pattern>(copy.maliciousPatterns);
	}
	
	/**
	 * add the elements of 'add' to this MPSM
	 * 
	 * @param add
	 */
	public void addAll(MPSM add) {
		if(!add.getMaliciousIPs().isEmpty()) {
			for (String ip : add.getMaliciousIPs()) {
				this.addIP(ip);
			}
		}
		if(!add.getMaliciousPatterns().isEmpty()) {
			for (Pattern pattern : add.getMaliciousPatterns()) {
				this.addPattern(pattern);
			}
		}
	}
	
	/**
	 * remove the elements of 'remove' from this MPSM
	 * 
	 * @param remove
	 */
	public void removeAll(MPSM remove) {
		if(!remove.getMaliciousIPs().isEmpty()) {
			for (String ip : remove.getMaliciousIPs()) {
				this.removeIP(ip);
			}
		}
		if(!remove.getMaliciousPatterns().isEmpty()) {
			for (Pattern pattern : remove.getMaliciousPatterns()) {
				this.removePattern(pattern);
			}
		}
	}

	/**
	 * add a malicious IP/Pattern to the right list
	 * 
	 * @param Malicious
	 */
	public void addMalicious(String Malicious) {
		if (!isIp(Malicious)) {
			this.addPattern(Pattern.compile(Malicious));
		} else {
			this.addIP(Malicious);
		}
	}
	
	/**
	 * remove a malicious IP/Pattern from the right list
	 * 
	 * @param Malicious
	 */
	public void removeMalicious(String Malicious) {
		if (!isIp(Malicious)) {
			this.removePattern(Pattern.compile(Malicious));
		} else {
			this.removeIP(Malicious);
		}
	}
	
	/**
	 * check if a malicious pattern is IP
	 * 
	 * @param ip
	 * @return
	 */
	public static boolean isIp(String ip) {
		Pattern ipPattern = Pattern
				.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		return ipPattern.matcher(ip).matches();
	}

	/**
	 * check if malicious IP exists in MPSM and if not adds it to the memory
	 * 
	 * @param malicious
	 * @return true if malicious is IP
	 */
	public void addIP(String malicious) {
		lock.writeLock().lock();
		if (!maliciousIPs.contains(malicious)) {
			maliciousIPs.add(malicious);
		}
		lock.writeLock().unlock();
	}

	/**
	 * check if malicious Pattern exists in MPSM and if not adds it to the
	 * memory
	 * 
	 * @param malicious
	 */
	public void addPattern(Pattern malicious) {
		lock.writeLock().lock();
		boolean newpattern = true;
		for (Pattern pat : maliciousPatterns) {
			if (pat.toString().equals(malicious.toString())) {
				newpattern = false;
			}
		}
		if (newpattern == true) {
			maliciousPatterns.add(malicious);
		}
		lock.writeLock().unlock();
	}

	/**
	 * getter for MaliciousIPs
	 * 
	 * @return List of Strings
	 */
	public List<String> getMaliciousIPs() {
		lock.readLock().lock();
		List<String> ret = this.maliciousIPs;
		lock.readLock().unlock();
		return ret;

	}

	/**
	 * getter for MaliciousPatterns
	 * 
	 * @return List of Strings
	 */
	public List<Pattern> getMaliciousPatterns() {
		lock.readLock().lock();
		List<Pattern> ret = this.maliciousPatterns;
		lock.readLock().unlock();
		return ret;
	}
	
	/**
	 * check if 'Malicious' exists in this MPSM
	 * 
	 * @param malicious
	 * @return
	 */
	public boolean contains(String malicious) {
		if (this.getMaliciousIPs().contains(malicious)) {
			return true;
		} else {
			if(this.getMaliciousPatterns().isEmpty()) {
				return false;
			}
			for (Pattern pattern : this.getMaliciousPatterns()) {
				if (pattern.toString().equals(malicious)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return maliciousIPs.hashCode() + maliciousPatterns.hashCode();
	}

	/**
	 * remove this malicious IP from MPSM
	 */
	public void removeIP(String malicious) {
		lock.writeLock().lock();
		maliciousIPs.remove(malicious);
		lock.writeLock().unlock();
	}

	/**
	 * remove this malicious Pattern from MPSM
	 */
	public void removePattern(Pattern malicious) {
		lock.writeLock().lock();
		boolean removed = false;
		if(!this.getMaliciousPatterns().isEmpty()) {
			int i = 0;
			for (Pattern pattern : this.getMaliciousPatterns()) {
				if (pattern.toString().equals(malicious.toString())) {
					removed = true;
					break;
				}
				i++;
			}
			if(removed) {
				maliciousPatterns.remove(i);
			}
		}
		lock.writeLock().unlock();
	}

	/**
	 * setter for maliciousIPs
	 * 
	 * @param maliciousIPs
	 */
	public void setMaliciousIPs(List<String> maliciousIPs) {
		lock.writeLock().lock();
		this.maliciousIPs = maliciousIPs;
		lock.writeLock().unlock();
	}

	/**
	 * setter for maliciousPatterns
	 * 
	 * @param maliciousPatterns
	 */
	public void setMaliciousPatterns(List<Pattern> maliciousPatterns) {
		lock.writeLock().lock();
		this.maliciousPatterns = maliciousPatterns;
		lock.writeLock().unlock();
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("	MPSM\n");
		ret.append("MaliciousIPs    Malicious String Patterns\n");
		lock.readLock().lock();
		if (!this.getMaliciousIPs().isEmpty()
				&& !this.getMaliciousPatterns().isEmpty()) {
			Iterator<String> it1 = this.getMaliciousIPs().iterator();
			Iterator<Pattern> it2 = this.getMaliciousPatterns().iterator();
			while (it1.hasNext() || it2.hasNext()) {
				String str;
				if (it1.hasNext()) {
					str = it1.next();
				} else
					str = "";
				for (int i = str.length(); i <= "MaliciousIPs   ".length(); i++)
					str += " ";
				ret.append(str);
				if (it2.hasNext()) {
					ret.append(it2.next() + "\n");
				} else {
					ret.append("\n");
				}
			}
		}
		lock.readLock().unlock();
		return ret.toString().trim();
	}

}
