package di.uoa.client.settings;

import di.uoa.settings.Settings;

/**
 * Class to take server location properties from property File
 * @see {@link Settings}
 * @author Kostis
 *
 */

public class ServerProperties {

	private String port;
	private String path;
	private String ip;
	private String rest;
	private static ServerProperties instance;
	
	/**
	 * static method to get rest path
	 * 
	 * @return rest path as String
	 */
	public static String getRestPath() {
		ServerProperties server = getInstance();
		String url = "http://" + server.ip + ":" + server.port + "/"
				+ server.path + "/" + server.rest;
		return url;
	}

	private static synchronized ServerProperties getInstance() {
		if (instance == null) {
			instance = new ServerProperties();
		}
		return instance;
	}

	private ServerProperties() {
		port = Settings.getValue("server.port");
		path = Settings.getValue("server.path");
		ip = Settings.getValue("server.ip");
		rest = Settings.getValue("server.rest");
	}
}
