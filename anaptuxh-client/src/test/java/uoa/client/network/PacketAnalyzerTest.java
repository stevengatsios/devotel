package uoa.client.network;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.Test;

import di.uoa.client.network.PacketAnalyser;
import di.uoa.memory.MPSM;

public class PacketAnalyzerTest {

	@Test
	public void testCount() {
		String testStr = "hellojellokelloIello";
		Pattern testPattern = Pattern.compile("[h-k]ello",
				Pattern.CASE_INSENSITIVE);
		int count = PacketAnalyser.count(testPattern, testStr);
		assertEquals(4, count);
	}
	
	@Test
	public void testIsIp() {
		String ip = "127.0.0.1";
		assertEquals(true, MPSM.isIp(ip));
	}
}
