package di.uoa.server.database.bases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import di.uoa.server.database.DAO;
import di.uoa.server.memory.Node;

public class ClientsBase {

	public static boolean Exists(String node) throws SQLException {
		ResultSet rs;
		boolean ret;
		rs = DAO.executeQuery("SELECT * FROM Clients WHERE "
				+ "idClient='" + node + "'");
		ret = rs.next();
		DAO.closeStatement();
		return ret;
	}
	
	/**
	 * Get All Clients from database
	 * @return List
	 */
	public static List<Node> getClients() {
		List<Node> ret = new ArrayList<Node>();
		ResultSet resultset;
		resultset = DAO.executeQuery("SELECT idClient,online FROM Clients;");
		try {
			while (resultset.next()) {
				String name = resultset.getString("idClient");
				boolean online = resultset.getBoolean("online");
				ret.add(new Node(name, online));
			}
			DAO.closeStatement();
		} catch (SQLException e) {
			System.err.println(e.getSQLState());
		}
		return ret;
	}
	
	/**
	 * Register a Client with nodeID to database
	 * 
	 * @param nodeID
	 * @return
	 */
	public static boolean register(String nodeID) {
		return DAO.executeUpdate("INSERT INTO Clients VALUES('" + nodeID
				+ "', true)");
	}
	
	/**
	 * Change Client's status to database
	 * 
	 * @param nodeID
	 * @param online
	 * @return
	 */
	public static boolean changeStatus(String nodeID, boolean online) {
		return DAO.executeUpdate("UPDATE Clients SET online=" + online  
				+ " WHERE idClient='" + nodeID + "'");
	}
}
