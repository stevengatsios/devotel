package di.uoa.server.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import di.uoa.server.database.bases.StatisticsBase;

public class MaliciousPOJO implements Comparable<MaliciousPOJO> {

	private String pattern;
	private int frequency;
	
	public MaliciousPOJO(String pattern, int frequency) {
		this.setPattern(pattern);
		this.setFrequency(frequency);
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	public static List<MaliciousPOJO> getMaliciousList() {
		ResultSet res = StatisticsBase.getStatistics();
		List<MaliciousPOJO> ret = new ArrayList<MaliciousPOJO>();
		try {
			while(res.next()) {
				String pat = res.getString("Malicious_pattern");
				int freq = res.getInt("Frequency");
				ret.add(new MaliciousPOJO(pat, freq));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return ret;
	}

	@Override
	public int compareTo(MaliciousPOJO o) {
		return this.frequency-o.frequency;
	}
}
