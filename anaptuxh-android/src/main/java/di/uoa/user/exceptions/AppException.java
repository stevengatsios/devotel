package di.uoa.user.exceptions;

/**
 * Exception 
 * 
 * @author Steve
 *
 */
public class AppException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8342910832286568325L;

	public AppException(String message) {
		super(message);
	}
	
	public AppException(Throwable e) {
		super(e);
	}
}
