package di.uoa.server.gui.panels;

import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ServerPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2377359256886547055L;

	private JLabel image;

	public ServerPanel() {
		try {
			InputStream icon = getClass().getClassLoader().getResourceAsStream(
					"icons/base.png");
			ImageIcon tmp = new ImageIcon(ImageIO.read(icon));
			icon.close();
			image = new JLabel(tmp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(this.getSize());
		add(image);
	}
}
