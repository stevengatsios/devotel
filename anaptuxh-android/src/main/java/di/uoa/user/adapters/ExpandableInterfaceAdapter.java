package di.uoa.user.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import di.uoa.user.R;
import di.uoa.user.db.bases.NodeBase;
import di.uoa.user.db.bases.StatisticBase;
import di.uoa.user.utils.Account;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
 
/**
 * Create a expandable list of UUID to interfaces
 * 
 * @author Kostas
 *
 */
public class ExpandableInterfaceAdapter extends AbstractExpandableAdapter {
 
    private HashMap<String, List<String>> _listDataChild;

	public ExpandableInterfaceAdapter(Context context, boolean myDevice) {
		super(context);
		if(myDevice) {
        	this._listDataHeader = new ArrayList<String>(Account.getAvailableNodes());
        } else {
        	NodeBase nb = new NodeBase(context);
        	nb.open();
        	this._listDataHeader = nb.getAllNodes();
        	nb.close();
        }
		StatisticBase sb = new StatisticBase(context);
		sb.open();
        this._listDataChild = sb.getAllInterfacesAsMap(this._listDataHeader);
        sb.close();
	}

	@Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
 
        final String childText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_interface_layout, parent, false);
        }
 
        TextView interfaceListChild = (TextView) convertView
                .findViewById(R.id.exp_interface);
 
        interfaceListChild.setText(childText);
        return convertView;
    }
	
	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
}
}