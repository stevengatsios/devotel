package di.uoa.user.db.tables;

import di.uoa.memory.S_MPSM.Register;

/**
 * Data Class to represents Statistics Table of database
 * 
 * @author Kostas
 *
 */
public class Statistic {
	private String node;
	private String inter;
	private String interfaceIP;
	private String malicious;
	private long frequency;
	private long id;

	public Statistic(long id, String node, Register reg) {
		this.node = node;
		this.inter = reg.getInterfaceName();
		this.interfaceIP = reg.getInterfaceIP();
		this.malicious = reg.getMaliciousPattern();
		this.frequency = reg.getFrequency();
		this.id = id;
	}
	
	public Statistic(long id, String node, String inter, String interfaceIP,
			String malicious, long frequency) {
		this.node = node;
		this.inter = inter;
		this.interfaceIP = interfaceIP;
		this.malicious = malicious;
		this.frequency = frequency;
		this.id = id;
	}
	
	public Statistic() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getInter() {
		return inter;
	}

	public void setInter(String inter) {
		this.inter = inter;
	}

	public String getInterfaceIP() {
		return interfaceIP;
	}

	public void setInterfaceIP(String interfaceIP) {
		this.interfaceIP = interfaceIP;
	}

	public String getMalicious() {
		return malicious;
	}

	public void setMalicious(String malicious) {
		this.malicious = malicious;
	}

	public long getFrequency() {
		return frequency;
	}

	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}
}
