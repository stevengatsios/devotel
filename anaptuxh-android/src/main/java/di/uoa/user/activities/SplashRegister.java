package di.uoa.user.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.memory.AvailableNodes;
import di.uoa.user.R;
import di.uoa.user.db.bases.NodeBase;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.tasks.UpdateService;
import di.uoa.user.utils.Accumulator;
import di.uoa.user.utils.ServerConnector;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Activity that handles user registration.
 * 
 * @author Kostas
 *
 */
public class SplashRegister extends Activity {

	private UserRegisterTask urt = null;

	LinearLayout containerLayout;
	private Intent mServiceIntent = null;
	static int device_num = 0;
	Button button;
	Button register;
	Context context;
	EditText username;
	EditText password;
	EditText confirm;
	List<String> devices;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		device_num = 0;
		containerLayout = (LinearLayout) findViewById(R.id.registration_devices);
		button = (Button) findViewById(R.id.enter_devices);
		devices = new ArrayList<String>();
		mServiceIntent = new Intent(Accumulator.getContext(), UpdateService.class);
		Accumulator.getContext().startService(mServiceIntent);

		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (device_num > 20) {
					return;
				}
				EditText editText = new EditText(getBaseContext());
				containerLayout.addView(editText);
				editText.setHint("Enter UUID");
				LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) editText
						.getLayoutParams();
				layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
				editText.setLayoutParams(layoutParams);
				editText.setTag("EditText" + device_num);
				device_num++;
			}
		});

		register = (Button) findViewById(R.id.register_button);

		register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (device_num > 0) {
					for (int i = 0; i < device_num; i++) {
						devices.add(((EditText) containerLayout.getChildAt(i))
								.getText().toString());
					}
				} else {
					devices = null;
				}
				boolean success = attempRegister(v);
				if (success) {
					Toast.makeText(getApplicationContext(),
							getString(R.string.succesfull_register), Toast.LENGTH_SHORT)
							.show();
					finish();
				}
				// clearForm();
			}

		});

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
			getMenuInflater().inflate(R.menu.global, menu);
			return super.onCreateOptionsMenu(menu);
		}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if(item.getItemId() == R.id.greek) {
			Locale locale = new Locale("gr"); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		if(item.getItemId() == R.id.english) {
			Locale locale = new Locale(""); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void restartActivity() {
	    Intent intent = getIntent();
	    finish();
	    startActivity(intent);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Accumulator.getContext().stopService(mServiceIntent);
	}

	/**
	 * Clears the registration fields.
	 */
	private void clearForm() {
		username.setText("");
		password.setText("");
		confirm.setText("");
	}

	public boolean attempRegister(View v) {
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		confirm = (EditText) findViewById(R.id.password_confirm);

		// Get user details.
		String user = username.getText().toString();
		String pass = password.getText().toString();
		String conf = confirm.getText().toString();
		// Check if all fields have been completed.
		if (user.equals("") || pass.equals("")) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.All_Fields),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		// Check password match.
		if (!pass.equals(conf)) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.error_incorrect_username), Toast.LENGTH_SHORT).show();
			password.setText("");
			confirm.setText("");
			password.requestFocus();
			return false;
		}
		AvailableNodes nodes = new AvailableNodes();
		if (device_num > 0) {
			for (int i = 0; i < device_num; i++) {
				String node = devices.get(i);
				if(!NodeExists(node)) {
					Toast.makeText(getApplicationContext(),
							node + getString(R.string.node_no_registered), Toast.LENGTH_SHORT).show();
					containerLayout.removeViewAt(i);
					device_num--;
					devices.remove(i);
					return false;
				}
				nodes.add(devices.get(i));
			}
		} else {
			Toast.makeText(getApplicationContext(),
					getString(R.string.devices_needed), Toast.LENGTH_SHORT).show();
			return false;
		}
		urt = new UserRegisterTask(username.getText().toString(), password
				.getText().toString(), nodes);
		urt.execute((Void) null);
		Boolean ret = false;
		try {
			ret = urt.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (ret == false) {
			clearForm();
			return false;
		}
		return true;
	}
	
	public boolean NodeExists(String node) {
		NodeBase nodes;
		boolean exists;
		nodes = new NodeBase(getApplicationContext());
		nodes.open();
		exists = nodes.exists(node);
		nodes.close();
		return exists;
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

		private final String usrname;
		private final String password;
		private final AvailableNodes nodes;
		private final Handler handle;

		UserRegisterTask(String user, String pass, AvailableNodes nodes) {
			this.usrname = user;
			this.password = pass;
			this.nodes = nodes;
			handle = new Handler(getApplication().getMainLooper());
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			ServerConnector serv = ServerConnector.getInstance();
			List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
			queryParams.add(new BasicNameValuePair("username", usrname));
			queryParams.add(new BasicNameValuePair("password", password));
			String resp;
			try {
				resp = serv.doPOST("credentials/android/register", queryParams,
						nodes, new TypeReference<String>() {});
			} catch (final AppException e) {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(), e.getMessage(),
								Toast.LENGTH_SHORT).show();
					}
				});
				return false;
			}

			if (resp == null) {
				Toast.makeText(getApplication(), getString(R.string.no_server_connection),
						Toast.LENGTH_SHORT).show();
				return false;
			} else if ("false".equals(resp)) {
				return false;
			} else if ("true".equals(resp)) {
				return true;
			} else {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(),
								R.string.no_server_connection,
								Toast.LENGTH_SHORT).show();
					}
				});
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			urt = null;

			if (success) {
				Log.i("POST EXECUTE", "success");
				Intent intent = new Intent(getApplication(), SplashLogin.class);
				finish();
				startActivity(intent);

			} else {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(),
								getString(R.string.error_incorrect_username),
								Toast.LENGTH_SHORT).show();
					}
				});
				username.setText("");
				username.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			urt = null;
		}
	}
}