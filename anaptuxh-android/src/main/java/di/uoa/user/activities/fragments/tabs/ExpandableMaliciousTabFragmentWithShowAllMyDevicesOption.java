package di.uoa.user.activities.fragments.tabs;

import di.uoa.user.R;
import di.uoa.user.adapters.AbstractExpandableAdapter;
import di.uoa.user.adapters.ExpandableMaliciousAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

/**
 * It accepts a bundle with
 * {@link ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption#ARGUMENT}
 * if its true it will show all devices else just ours.
 */
public class ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption extends Fragment {
	public final static String ARGUMENT = "myDevices";
	ExpandableListView expListView;
	boolean myDevices;
	private AbstractExpandableAdapter listAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.myDevices = getArguments().getBoolean(ARGUMENT);
		setHasOptionsMenu(false);
		View rootView = inflater.inflate(R.layout.exp_list_layout, container,
				false);
		expListView = (ExpandableListView) rootView
				.findViewById((R.id.exp_list_view));
		listAdapter = new ExpandableMaliciousAdapter(getActivity(), myDevices);
		expListView.setAdapter(listAdapter);
		return rootView;
	}
}