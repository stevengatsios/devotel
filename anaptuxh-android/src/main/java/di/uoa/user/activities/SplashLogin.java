package di.uoa.user.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.user.R;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.tasks.AvailableNodesFuture;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.ServerConnector;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity that handles attempts for log in.
 * 
 * @author Steve
 *
 */
public class SplashLogin extends Activity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// UI references.
	private EditText mUsernameView;
	private EditText mPasswordView;
	private View mProgressView;
	private View mLoginFormView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		// Set up the login form.
		
		mUsernameView = (EditText) findViewById(R.id.username);
		String username = Account.getUsername();
		mUsernameView.setText(username, EditText.BufferType.EDITABLE);

		// password
		mPasswordView = (EditText) findViewById(R.id.password);
		String pass = Account.getPassword();
		mPasswordView.setText(pass, EditText.BufferType.EDITABLE);

		mLoginFormView = findViewById(R.id.credentials);
		mProgressView = findViewById(R.id.login_progress);

		// if you have entered them before, attempt login.
		if (username.length() != 0 && pass.length() != 0) {
			Intent offlineIntent = new Intent(getApplicationContext(),HomeActivity.class);
			startActivity(offlineIntent);
			finish();
		}
		Button mLoginButton = (Button) findViewById(R.id.login_button);
		mLoginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				attemptLogin();
			}
		});

		Button mRegisterButton = (Button) findViewById(R.id.go_to_register);
		mRegisterButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplication(), SplashRegister.class);
				startActivity(intent);
				
			}
		});

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
			getMenuInflater().inflate(R.menu.global, menu);
			return super.onCreateOptionsMenu(menu);
		}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if(item.getItemId() == R.id.greek) {
			Locale locale = new Locale("gr"); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		if(item.getItemId() == R.id.english) {
			Locale locale = new Locale(""); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void restartActivity() {
	    Intent intent = getIntent();
	    finish();
	    startActivity(intent);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mUsernameView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		String username = mUsernameView.getText().toString();
		String password = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(username)) {
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		} else if (!isUsernameValid(username)) {
			mUsernameView.setError(getString(R.string.error_invalid_email));
			focusView = mUsernameView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			showProgress(true);
			mAuthTask = new UserLoginTask(username, password);
			mAuthTask.execute((Void) null);
		}
	}

	private boolean isUsernameValid(String username) {
		// TODO: Replace this with your own logic
		return username.length() > 1;
	}

	private boolean isPasswordValid(String password) {
		// TODO: Replace this with your own logic
		return password.length() > 1;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		private final String mUsername;
		private final String mPassword;
		private final Handler handle;

		UserLoginTask(String username, String password) {
			mUsername = username;
			mPassword = password;
			handle = new Handler(getApplication().getMainLooper());
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			ServerConnector serv = ServerConnector.getInstance();
			List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
			queryParams.add(new BasicNameValuePair("username", mUsername));
			String resp;
			try {
				resp = serv.doPOST("credentials/android/login", queryParams,
						mPassword, new TypeReference<String>() {
						});
			} catch (final AppException e) {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(), e.getMessage(),
								Toast.LENGTH_SHORT).show();
					}
				});
				return false;
			}
			Log.i("LOGIN", "Server responed with " + resp);
			if (resp == null) { // server is down ?
				return false;
			} else if ("0".equals(resp)) {
				Account.setPassword(null);
				return false;
			} else {
				int accountType = Integer.parseInt(resp);
				Account.setUsername(mUsername);
				Account.setPassword(mPassword);
				if (accountType == 1) { // TODO Normal User
					Account.setAdmin(false);
					return true;
				} else if (accountType == 2) { // TODO Admin User
					Account.setAdmin(true);
					return true;
				} else {
					return false;
				}
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				// Get the nodes
				AvailableNodesFuture nodes = new AvailableNodesFuture();
				nodes.execute((Void) null);
				try {
					nodes.get();
				} catch (InterruptedException | ExecutionException e) {
					Log.e(nodes.getClass().toString(), e.getMessage());
				}
				Intent intent = new Intent(getApplication(), HomeActivity.class);
				finish();
				startActivity(intent);

			} else {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(),
								getString(R.string.error_incorrect_password),
								Toast.LENGTH_SHORT).show();
					}
				});
				mPasswordView.setText("");
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
