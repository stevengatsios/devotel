package di.uoa.client;

import java.util.Timer;

import di.uoa.client.network.thread.InterfaceSniffer;
import di.uoa.client.network.thread.NetworkInterfaceDetection;
import di.uoa.client.register.thread.PCRegister;
import di.uoa.client.thread.MemorySynchronizer;
import di.uoa.client.thread.ServerCommunication;
import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;
/**
 * Creates an abstraction layer (facade) to use complicated ${@link Client}
 * 
 * @author Steve
 */
public class ClientFacade {

	/**
	 * Thread that handles the termination of execution of this program.
	 * 
	 * @author Steve
	 *
	 */
	public class ExitThread extends Thread {

		@Override
		public void run() {
			System.out.println("Termination sequence initiated...");
			memorySync.interrupt();
			if (detector != null) {
				detector.interrupt();
			}
			if (serverUpdates != null) {
				serverUpdates.cancel();
			}
			try {
				memorySync.join();
				if (detector != null) {
					detector.join();
				}
				synchronized (register.lockUnregister) {
					register.lockUnregister.notify();
				}
				if (register.isAlive()) {
					register.join();
				}
			} catch (InterruptedException e) {
				return;
			}
			System.out.println("Exiting");
		}
	}

	static ClientFacade instance = null;

	/**
	 * 
	 * @return {@link ClientFacade} singleton instance.
	 */
	public static ClientFacade getInstance() {
		if (instance == null) {
			instance = new ClientFacade();
		}
		return instance;
	}

	public NetworkInterfaceDetection detector;
	public Thread exitThread;
	public MemorySynchronizer memorySync;
	public MPSM mpsm;
	public PCRegister register;

	public S_MPSM s_mpsm;

	Timer serverUpdates;

	/**
	 * Private Constructor for singleton class.
	 */
	private ClientFacade() {
		exitThread = new ExitThread();
		Runtime.getRuntime().addShutdownHook(exitThread);
		mpsm = new MPSM();
		s_mpsm = new S_MPSM();
		memorySync = new MemorySynchronizer(mpsm, s_mpsm);
	}

	/**
	 * Handles the registration to the accumulator (server).
	 * 
	 * @throws InterruptedException
	 */
	public void register() throws InterruptedException {
		register = new PCRegister();
		register.start();
		synchronized (register.lockRegister) {
			register.lockRegister.wait();
		}
		if(!register.isOK()) {
			System.exit(1);
		}
	}

	/**
	 * Handles the synchronization of the memories with the accumulator
	 * (server).
	 * 
	 * @throws InterruptedException
	 */
	public void serverSync() throws InterruptedException {
		memorySync.start();
		// wait until memory is initialized
		synchronized (memorySync) {
			memorySync.wait();
		}
		serverUpdates = new Timer();
		serverUpdates.scheduleAtFixedRate(new ServerCommunication(mpsm), 0,
				ServerCommunication.INTERVAL);
	}

	/**
	 * Starts the {@link NetworkInterfaceDetection} that handles Network
	 * interfaces and controls the group pf {@link InterfaceSniffer} threads.
	 */
	public void startSniffing() {
		detector = new NetworkInterfaceDetection(mpsm, s_mpsm);
		detector.start();
	}

	/**
	 * Evokes the {@link ExitThread} that terminates the program smoothly.
	 */
	public void terminate() {
		exitThread.start();
	}
}
