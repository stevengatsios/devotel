package di.uoa.client.network.thread;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

import di.uoa.client.network.NetworkInterfaceManager;
import di.uoa.client.network.PacketHandler;
import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;

/**
 * Thread that opens the interface and captures packets online.
 * 
 * @author Kostas, Steve
 *
 */
public class InterfaceSniffer extends Thread {
	private PcapIf deviceInterface = null;
	private PacketHandler handler = null;
	MPSM mpsm = null;
	private Pcap pcap = null;
	S_MPSM smpsm = null;
	private boolean sniffing = false;

	public InterfaceSniffer(ThreadGroup thGp, PcapIf inter, MPSM mpsm,
			S_MPSM smpsm) {
		super(thGp, inter.getName());
		deviceInterface = inter;
		this.mpsm = mpsm;
		this.smpsm = smpsm;
	}

	public Pcap getPcap() {
		return pcap;
	}

	/**
	 * Check if this interface has IP address.
	 * 
	 * @return true or false
	 */

	private boolean isOnline() {
		for (PcapIf inter : NetworkInterfaceManager.findNetworkDevices()) {
			if (inter.getName().equals(deviceInterface.getName())) {
				deviceInterface = inter;
				if (NetworkInterfaceManager.getInetAddress(deviceInterface) != null) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	public boolean isSniffing() {
		return sniffing;
	}

	@Override
	public void run() {
		String description = (deviceInterface.getDescription() != null) ? deviceInterface
				.getDescription() : "No description available";
		System.out.printf("I'm a new thread for %s\n",
				(System.getProperty("os.name").toLowerCase()
						.contains("windows")) ? deviceInterface.getName() + "["
						+ description + "]" : deviceInterface.getName());
		StringBuilder errbuf = new StringBuilder();
		int snaplen = 64 * 1024; // Capture all packets, no truncation
		int flags = Pcap.MODE_PROMISCUOUS; // capture all packets
		int timeout = 2 * 1000; // 2 seconds in milliseconds
		try {
			while (!this.isInterrupted()) {
				Thread.sleep(500);
				if (!this.isOnline()) {
					continue;
				} else {
					pcap = Pcap.openLive(deviceInterface.getName(), snaplen,
							flags, timeout, errbuf);
					if (pcap == null) {
						System.err
								.printf("Error while opening device for capture: "
										+ errbuf.toString());
						break;
					}
					this.sniffing = true;
					System.out.println(this.getName()
							+ " is online, starts sniffing");
					handler = new PacketHandler(mpsm, smpsm, deviceInterface);
					pcap.loop(Pcap.LOOP_INFINITE, handler, null);
					pcap.close();
					break;
				}
			}
		} catch (InterruptedException e) {
		}
		System.out.println("Terminated " + this.getName());
	}

}
