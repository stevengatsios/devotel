package di.uoa.server.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import di.uoa.server.gui.panels.IpPanel;
import di.uoa.server.gui.panels.PatternPanel;
import di.uoa.server.gui.panels.RemovedIpPanel;
import di.uoa.server.gui.panels.RemovedPatternPanel;
import di.uoa.server.gui.panels.ServerPanel;
import di.uoa.server.gui.panels.StatisticsPanel;
import di.uoa.server.gui.panels.UsersPanel;

public class Admin extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2341742985349937975L;
	JPanel mainPanel = new JPanel();
	JLabel statusbar = new JLabel(ServerRunner.INACTIVE_STATUS);
	/**
	 * Thread container for server;
	 */
	private static ServerRunner server;
	/**
	 * Constructor
	 */
	
	JButton serverToggle;
	public Admin() {

		server = null;
		try {
			initUI();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Create GUI window with a left vertical tool bar.
	 * 
	 * @throws IOException
	 */
	public final void initUI() throws IOException {
		// create toolbar
		JToolBar toolbar = new JToolBar(JToolBar.VERTICAL);
		toolbar.setFloatable(false);

		// create first option
		InputStream stream = getClass().getClassLoader().getResourceAsStream(
				"icons/computer-user.png");
		ImageIcon usr = new ImageIcon(ImageIO.read(stream));
		stream.close();
		JButton users = new JButton(usr);
		users.setBorder(new EmptyBorder(0, 0, 0, 0));
		users.setToolTipText("Show Users");
		toolbar.add(users);
		users.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mainPanel.removeAll();
				UsersPanel usrs = new UsersPanel();
				mainPanel.add(usrs, BorderLayout.CENTER);
				SwingUtilities.updateComponentTreeUI(mainPanel);
			}
		});

		toolbar.addSeparator();

		// create second option
		stream = getClass().getClassLoader().getResourceAsStream(
				"icons/add-to-database.png");
		ImageIcon addData = new ImageIcon(ImageIO.read(stream));
		stream.close();
		JButton data = new JButton(addData);
		data.setBorder(new EmptyBorder(0, 0, 0, 0));
		data.setToolTipText("Update Patterns");
		toolbar.add(data);
		data.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mainPanel.removeAll();
				IpPanel ip = new IpPanel();
				mainPanel.add(ip, BorderLayout.CENTER);
				PatternPanel pattern = new PatternPanel();
				mainPanel.add(pattern, BorderLayout.CENTER);
				SwingUtilities.updateComponentTreeUI(mainPanel);
			}
		});

		toolbar.addSeparator();

		// create third option
		stream = getClass().getClassLoader().getResourceAsStream(
				"icons/database.png");
		ImageIcon showData = new ImageIcon(ImageIO.read(stream));
		stream.close();
		JButton getData = new JButton(showData);
		getData.setBorder(new EmptyBorder(0, 0, 0, 0));
		getData.setToolTipText("Statistics");
		toolbar.add(getData);
		getData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mainPanel.removeAll();
				StatisticsPanel stats = new StatisticsPanel();
				mainPanel.add(stats, BorderLayout.CENTER);
				SwingUtilities.updateComponentTreeUI(mainPanel);
			}
		});

		toolbar.addSeparator();

		// create fourth option
		stream = getClass().getClassLoader().getResourceAsStream(
				"icons/trash.png");
		ImageIcon removeData = new ImageIcon(ImageIO.read(stream));
		stream.close();
		JButton removeMalicious = new JButton(removeData);
		removeMalicious.setBorder(new EmptyBorder(0, 0, 0, 0));
		removeMalicious.setToolTipText("Recycle Bin");
		toolbar.add(removeMalicious);
		removeMalicious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mainPanel.removeAll();
				RemovedIpPanel removedIps = new RemovedIpPanel();
				mainPanel.add(removedIps, BorderLayout.CENTER);
				RemovedPatternPanel removedPatterns = new RemovedPatternPanel();
				mainPanel.add(removedPatterns, BorderLayout.CENTER);
				SwingUtilities.updateComponentTreeUI(mainPanel);
			}
		});

		toolbar.addSeparator();

		// create fifth option
		stream = getClass().getClassLoader().getResourceAsStream(
				"icons/server-offline.png");
		ImageIcon startServer = new ImageIcon(ImageIO.read(stream));
		stream.close();
		serverToggle = new JButton(startServer);
		serverToggle.setBorder(new EmptyBorder(0, 0, 0, 0));
		serverToggle.setToolTipText("Start/Stop Server");
		toolbar.add(serverToggle);
		serverToggle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mainPanel.removeAll();
				ServerPanel serverPanel = new ServerPanel();
				mainPanel.add(serverPanel);
				SwingUtilities.updateComponentTreeUI(mainPanel);
				if(server == null) {
					try {
						server = new ServerRunner();
						server.start();
						statusbar.setText(ServerRunner.RUNNING_STATUS);
						InputStream icon = getClass().getClassLoader().getResourceAsStream(
								"icons/server.png");
						ImageIcon startServer = new ImageIcon(ImageIO.read(icon));
						icon.close();
						serverToggle.setIcon(startServer);
					} catch (IOException e) {
						e.printStackTrace();
					} catch(RuntimeException bind) {
						statusbar.setText(bind.getMessage());
					}
				} else {
					
					try {
						synchronized (server) {
							server.notify();
							server.join();
							statusbar.setText(ServerRunner.INACTIVE_STATUS);
							server = null;
						}
						InputStream icon = getClass().getClassLoader().getResourceAsStream(
								"icons/server-offline.png");
						ImageIcon startServer = new ImageIcon(ImageIO.read(icon));
						icon.close();
						serverToggle.setIcon(startServer);
					} catch (Exception e) {
						e.printStackTrace();
					} 
				}
			}
		});

		toolbar.addSeparator();

		add(toolbar, BorderLayout.WEST);
		add(mainPanel, BorderLayout.CENTER);

		// server status
		statusbar.setPreferredSize(new Dimension(-1, 22));
		statusbar.setBorder(LineBorder.createGrayLineBorder());
		add(statusbar, BorderLayout.SOUTH);

		setSize(700, 430);
		setTitle("Administrator Tools");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				Admin ex = new Admin();
				ex.setVisible(true);
			}
		});
	}
}