package di.uoa.server.gui.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import di.uoa.server.database.DAO;
import di.uoa.server.database.bases.MaliciousBase;


/**
 * 
 * @authors Kostis, Kostas
 *
 */
public class RemovedPatternPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JList<String> list;
	DefaultListModel<String> model;

	/**
	 * Constructor - Create a {@link JPanel} that displays Removed Patterns and has 
	 * the options Restore Pattern and Delete Pattern.
	 */
	public RemovedPatternPanel() {
		setLayout(new BorderLayout());
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		list.setVisibleRowCount(15);
		JScrollPane pane = new JScrollPane(list);
		JButton restoreButton = new JButton("Restore Pattern");
		JButton deleteButton = new JButton("Delete Pattern");
		for (String pattern : MaliciousBase.getMaliciousPatterns(false)) {
			model.addElement(pattern);
		}

		restoreButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (model.getSize() > 0) {
					String restore = list.getSelectedValue();
					if (restore != null) {
						int confirm = JOptionPane.showConfirmDialog(list,
								"Are you sure you wish to restore '" + restore
										+ "' ?");
						if (restore != null && confirm == 0) {
							model.removeElement(restore);
							DAO.executeUpdate("UPDATE Malicious SET valid=true"
									+ " WHERE pattern='" + restore + "'");
						}
					}
				}
			}
		});

		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (model.getSize() > 0) {
					String remove = list.getSelectedValue();
					if (remove != null) {
						int confirm = JOptionPane
								.showConfirmDialog(list,
										"Deleting '" + remove + "' will also remove the corresponding"
												+ " records from the Statistics.\n Continue ?");
						if (remove != null && confirm == 0) {
							model.removeElement(remove);
							DAO.executeUpdate("DELETE FROM Statistics"
									+ " WHERE Malicious_pattern='" + remove
									+ "'");
							DAO.executeUpdate("DELETE FROM Malicious"
									+ " WHERE pattern='" + remove + "'");
						}
					}
				}
			}
		});

		add(pane, BorderLayout.NORTH);
		add(restoreButton, BorderLayout.WEST);
		add(deleteButton, BorderLayout.EAST);
	}
}
