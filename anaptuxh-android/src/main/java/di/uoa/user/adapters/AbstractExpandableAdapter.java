package di.uoa.user.adapters;

import java.util.List;

import di.uoa.user.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

/**
 * Abstract class, children classes should implement their own child system.
 * @author Steve
 *
 */
public abstract class AbstractExpandableAdapter extends BaseExpandableListAdapter {

	private Context _context;
    protected List<String> _listDataHeader; // header titles

    
    public AbstractExpandableAdapter(Context context) {
        this._context = context;
    }
  
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
  
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_uuid_layout, parent, false);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.exp_uuid);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    
    public Context getContext() {
    	return this._context;
    }

}
