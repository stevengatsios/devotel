package di.uoa.server.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import di.uoa.memory.MPSM;
import di.uoa.memory.MaliciousPatterns;
import di.uoa.server.database.bases.MaliciousBase;
import di.uoa.server.memory.Node;
import di.uoa.server.memory.ServerMemory;

/**
 * Web Service Class for requests
 * 
 * @author Kostis
 *
 */
@Path("/requests")
public class Patterns {

	/**
	 * Send a request to the Server and receives changes in MPSM
	 * 
	 * @param nodeID
	 * @return MaliciousPatterns
	 */
	@GET
	@Path("/malicious")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public MaliciousPatterns maliciousPatternRequest(
			@QueryParam("id") String nodeID) {
		MaliciousPatterns malicious = new MaliciousPatterns();
		ServerMemory memory = ServerMemory.getInstance();
		Node node = memory.getNodeByID(nodeID);
		if (node == null) {
			return null;
		}
		malicious.setDeleted(new MPSM(node.getMpsm()));
		List<String> maliciousBase = MaliciousBase.getMalicious(true);
		for (String pattern : maliciousBase) {
			if (!node.getMpsm().contains(pattern)) {
				malicious.getAdded().addMalicious(pattern);
			} else {
				malicious.getDeleted().removeMalicious(pattern);
			}
		}
		node.getMpsm().addAll(malicious.getAdded());
		node.getMpsm().removeAll(malicious.getDeleted());
		return malicious;
	}
	
}
