package di.uoa.server.gui.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import di.uoa.server.database.bases.ClientsBase;
import di.uoa.server.memory.Node;

/**
 * 
 * @author Kostas
 *
 */
public class UsersPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2589379682195023781L;
	JList<String> list;
	DefaultListModel<String> model;

	/**
	 * Constructor - Create a {@link JPanel} that displays Users.
	 * 
	 */
	public UsersPanel() {
		setLayout(new BorderLayout());
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		JScrollPane pane = new JScrollPane(list);

		JButton allUsers = new JButton("All Users");
		JButton onlineUsers = new JButton("Online Users");
		JButton offlineUsers = new JButton("Offline Users");

		for (Node client : ClientsBase.getClients()) {
			model.addElement(client.getUserID());
		}
		list = new JList<String>(model);

		allUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.removeAllElements();
				for (Node client : ClientsBase.getClients()) {
					model.addElement(client.getUserID());
				}
				list = new JList<String>(model);
			}
		});

		onlineUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.removeAllElements();
				for (Node client : ClientsBase.getClients()) {
					if (client.isOnline()) {
						model.addElement(client.getUserID());
					}
					list = new JList<String>(model);
				}
			}
		});

		offlineUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.removeAllElements();
				list = new JList<String>(model);
				for (Node client : ClientsBase.getClients()) {
					if (!client.isOnline()) {
						model.addElement(client.getUserID());
					}
					list = new JList<String>(model);
				}
			}
		});
		list.setVisibleRowCount(15);
		pane = new JScrollPane(list);
		add(pane, BorderLayout.NORTH);
		add(allUsers, BorderLayout.WEST);
		add(onlineUsers, BorderLayout.CENTER);
		add(offlineUsers, BorderLayout.EAST);
	}
}
