package di.uoa.memory;

import java.util.ArrayList;

/**
 * Container for server resources
 * @author steve_000
 * INFO Removed it from server-core, it messes up android project because of the sql connector
 */
public class AvailableNodes extends ArrayList<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1903558680409313563L;

}
