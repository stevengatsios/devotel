package di.uoa.client.thread;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.jackson.JacksonFeature;

import di.uoa.client.register.UserID;
import di.uoa.client.settings.ServerProperties;
import di.uoa.memory.MPSM;
import di.uoa.memory.MaliciousPatterns;
import di.uoa.memory.S_MPSM;
import di.uoa.memory.StatisticalReports;
import di.uoa.settings.Settings;

/**
 * Initialize MPSM memory and send two memories to the accumulator every 
 * INTERVAL milliseconds
 * 
 * @authors Kostis
 */

public class MemorySynchronizer extends Thread {

	static private int INTERVAL;
	private MPSM mpsm = null;
	private S_MPSM s_mpsm = null;
	private Client client;

	public MemorySynchronizer(MPSM mpsm, S_MPSM s_mpsm) {
		super("memory initializer thread");
		this.mpsm = mpsm;
		this.s_mpsm = s_mpsm;
		INTERVAL = Integer.parseInt(Settings.getValue("memory.update"));
		//ClientConfig conf = new DefaultClientConfig(JacksonFeature.class);
		client = ClientBuilder.newClient().register(JacksonFeature.class);
	}

	/**
	 * Getter for mpsm
	 * 
	 * @return mpsm
	 */
	public MPSM getMPSM() {
		return mpsm;
	}

	/**
	 * Getter for s_mpsm
	 * 
	 * @return s_mpsm
	 */
	public S_MPSM getS_MPSM() {
		return s_mpsm;
	}

	/**
	 * Send request for malicious patterns to the Server.
	 */
	private MaliciousPatterns sendRequest() {
		WebTarget webResource = client.target(
				ServerProperties.getRestPath() + "/requests/malicious")
				.queryParam("id", UserID.getInstance().getUid());
		return webResource.request(MediaType.TEXT_PLAIN)
				.accept(MediaType.APPLICATION_JSON)
				.get(MaliciousPatterns.class);
	}

	/**
	 * Send {@link StatisticalReports} to the accumulator.
	 */
	private void sendStatisticalReport() {
		if (!s_mpsm.isEmpty()) {
			WebTarget webResource = client.target(
					ServerProperties.getRestPath() + "/reports/statistics")
					.queryParam("id", UserID.getInstance().getUid());

			StatisticalReports statistics = new StatisticalReports(s_mpsm);

            webResource.request().post(
                    Entity.entity(statistics, MediaType.APPLICATION_JSON),
                    String.class);


			System.out.println("S_MPSM sent successfully");
			s_mpsm.clear();
		}

	}

	/**
	 * Initialize MPSM from Server
	 *
	 */
	private void initialize() {
		MaliciousPatterns malicious = this.sendRequest();
		mpsm.addAll(malicious.getAdded());
		mpsm.removeAll(malicious.getDeleted());
		System.out.println("MPSM has been initiliazed successfully");
	}

	/**
	 * Starts the thread, initialize() first ,then notify that is done. Send two
	 * memories to the Server every {@link MemorySynchronizer.INTERVAL}
	 */
	@Override
	public void run() {
		this.initialize();
		// Notify that its finished
		synchronized (this) {
			this.notify();
		}

		try {
			while (!this.isInterrupted()) {
				Thread.sleep(INTERVAL);
				this.sendStatisticalReport();
			}
		} catch (InterruptedException e) {
			// TODO terminate
			System.out.println("Terminated " + this.getName());
		}
	}

	/**
	 * Setter for mpsm
	 * 
	 * @param mpsm
	 */
	public void setMPSM(MPSM mpsm) {
		this.mpsm = mpsm;
	}

	/**
	 * Setter for s_mpsm
	 * 
	 * @param s_mpsm
	 */
	public void setS_MPSM(S_MPSM s_mpsm) {
		this.s_mpsm = s_mpsm;
	}

}
