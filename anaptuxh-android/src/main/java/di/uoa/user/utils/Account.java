package di.uoa.user.utils;

import java.util.Set;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Utility Class to Save user's account settings when he is logged in
 * 
 * @author Steve
 *
 */
public class Account {

	private static SharedPreferences sp = PreferenceManager
			.getDefaultSharedPreferences(Accumulator.getInstance());
	
	public static String getUsername() {
		return sp.getString("username", "");
	}
	
	public static String getPassword() {
		return sp.getString("password", "");
	}
	
	public static void setUsername(String username) {
		sp.edit().putString("username", username).apply();;
	}
	
	public static void setPassword(String password) {
		sp.edit().putString("password", password).apply();
	}

	public static boolean getAdmin() {
		return sp.getBoolean("admin", false);
	}
	
	public static void setAdmin(boolean admin) {
		sp.edit().putBoolean("admin", admin).apply();;
	}
	
	/**
	 * 
	 * @return the available nodes of the user, else null
	 */
	public static Set<String> getAvailableNodes() {
		return sp.getStringSet("available_nodes", null);
	}
	
	/**
	 * Sets the available Nodes
	 * @param set
	 */
	public static void setAvailableNodes(Set<String> set) {
		sp.edit().putStringSet("available_nodes", set).apply();;
	}
	
}
