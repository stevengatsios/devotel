package di.uoa.user.utils;

/**
 * Container class for menu items. It uses the application context so as to pass
 * ids.
 * 
 * @author Steve
 *
 */
public class DrawerMenu {
	private int title;
	private int iconId;
	private int color;

	public DrawerMenu(int title, int iconId, int color) {
		this.title = title;
		this.iconId = iconId;
		this.color = color;
	}

	public int getTitle() {
		return title;
	}

	public void setTitle(int title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

}
