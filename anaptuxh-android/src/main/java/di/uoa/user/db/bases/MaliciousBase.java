package di.uoa.user.db.bases;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import di.uoa.user.db.MySqliteHelper;
import di.uoa.user.db.tables.Malicious;

/**
 * Utility Class to retrieve/update data from/to Malicious Table of database
 * 
 * @author Kostis
 *
 */
public class MaliciousBase {
	
	private SQLiteDatabase database;
	private MySqliteHelper dbHelper;
	private String[] allColumns = { MySqliteHelper.COLUMN_ID,
			MySqliteHelper.COLUMN_PATTERN };
	
	public MaliciousBase(Context context) {
		dbHelper = new MySqliteHelper(context);
	}
	
	/**
	 * open database
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	
	/**
	 * close database
	 */
	public void close() {
		dbHelper.close();
	}
	
	/**
	 * Create a new Malicious pattern {@param pattern}
	 * 
	 * @param pattern
	 * @return
	 */
	public Malicious createMalicious(String pattern) {
		ContentValues values = new ContentValues();
		values.put(MySqliteHelper.COLUMN_PATTERN, pattern);
		long insertId = database.replace(MySqliteHelper.TABLE_MALICIOUS, null,
				values);
		Cursor cursor = database.query(MySqliteHelper.TABLE_MALICIOUS, allColumns,
				MySqliteHelper.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		Malicious malicious= cursorToMalicious(cursor);
		cursor.close();
		return malicious;
	}
	
	/**
	 * Delete Malicious pattern {@param pattern}
	 * 
	 * @param pattern
	 * @return
	 */
	public void deleteMalicious(Malicious malicious) {
		long id = malicious.getId();
		database.delete(MySqliteHelper.TABLE_MALICIOUS, MySqliteHelper.COLUMN_ID
				+ " = " + id, null);
	}
	
	/**
	 * Delete All Malicious patterns from the Table
	 */
	public void deleteAllMalicious() {
		database.delete(MySqliteHelper.TABLE_MALICIOUS, null, null);
	}
	
	/**
	 * Retrieve All Malicious patterns as String
	 * 
	 * @return
	 */
	public List<String> getAllMalicious() {
		List<String> allmalicious = new ArrayList<String>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_MALICIOUS, allColumns,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Malicious malicious = cursorToMalicious(cursor);
			allmalicious.add(malicious.getPattern());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return allmalicious;
	}
	
	/**
	 * Check if Malicious pattern {@param pattern} exists in the Table
	 * 
	 * @param pattern
	 * @return
	 */
	public boolean exists(String pattern) {
		boolean exists;
		Cursor cursor = database.query(MySqliteHelper.TABLE_MALICIOUS, allColumns,
				MySqliteHelper.COLUMN_PATTERN + "='" + pattern + "'", null, null, null, null);
		exists = cursor.moveToFirst();
		cursor.close();
		return exists;
	}
	
	/**
	 * Convert cursor to a Malicious structure
	 * 
	 * @param cursor
	 * @return
	 */
	private Malicious cursorToMalicious(Cursor cursor) {
		Malicious malicious = new Malicious();
		malicious.setId(cursor.getLong(0));
		malicious.setPattern(cursor.getString(1));
		return malicious;
	}
}
