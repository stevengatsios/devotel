package di.uoa.user.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.memory.S_MPSM.Register;
import di.uoa.memory.StatisticalReports;
import di.uoa.user.db.bases.MaliciousBase;
import di.uoa.user.db.bases.NodeBase;
import di.uoa.user.db.bases.StatisticBase;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.Connectivity;
import di.uoa.user.utils.ServerConnector;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

/**
 * Service Task to Update Android database
 * 
 * @author Kostis
 *
 */
public class UpdateService extends Service{
    
	private Thread t;
    private boolean flag = false;
    private NodeBase nodes = null;
    private MaliciousBase malicious = null;
    private StatisticBase stats = null;
    
    private static final String TAG = "UpdateService";
    private static final int timer = 60 * 1000; // 1 minute 

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        nodes = new NodeBase(getApplicationContext());
        malicious = new MaliciousBase(getApplicationContext());
        stats = new StatisticBase(getApplicationContext());
        flag = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	//super.onStartCommand(intent, flags, startId);
    	t = new Thread(new Runnable() {

            @Override
            public void run() {
                while (flag) {
                    try {
                    	if(Connectivity.isConnected(getApplicationContext())) {
	                    	updateDatabase();	                     
	                        Log.d(TAG, "Update");
                    	}
                    	Thread.sleep(timer);
                    } catch (Exception e) {
                        e.printStackTrace();
                        flag = false;
                    }
                }
            }
        });
        t.start();
 
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        flag = false;
    }

    /**
     * Get data from Server and insert them in Android database
     */
    public void updateDatabase() {
    	ServerConnector connector = ServerConnector.getInstance();
    	List<StatisticalReports> reports;
    	String malicious;
    	List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
    	queryParams.add(new BasicNameValuePair("username", Account.getUsername()));
    	queryParams.add(new BasicNameValuePair("password", Account.getPassword()));
    	try {
			reports = connector.doGET("reports/android/statistics", 
					queryParams, new TypeReference<List<StatisticalReports>>(){});
			updateNodes(reports);
			updateStatistic(reports);
			malicious = connector.doGET("admin/tools/malicious", 
	    			queryParams, new TypeReference<String>(){});
	    	updateMalicious(malicious);
		} catch (AppException e) {
			System.out.println(e.getMessage());
		}
    }
    
    /**
     * Update Nodes Table of the database
     * 
     * @param reports
     */
    public void updateNodes(List<StatisticalReports> reports) {
    	nodes.open();
    	nodes.deleteAllNodes();
    	for(StatisticalReports report : reports) {
    		nodes.createNode(report.getClient());
    	}
    	nodes.close();
    }
    
    /**
     * Update Malicious Table of the database
     * 
     * @param newmalicious
     */
    public void updateMalicious(String newmalicious) {
    	malicious.open();
    	malicious.deleteAllMalicious();
    	String[] patterns = TextUtils.split(newmalicious, ",");
    	for(String pattern : patterns) {
    		malicious.createMalicious(pattern);
    	}
    	malicious.close();
    }
    
    /**
     * Update Statistics Table of the database
     * 
     * @param reports
     */
    public void updateStatistic(List<StatisticalReports> reports) {
    	stats.open();
    	stats.deleteAllStatistics();
    	for(StatisticalReports report : reports) {
    		for(Register register : report.getS_MPSM()) {
    			stats.createStatistic(report.getClient(), register);
    		}
    	}
    	stats.close();
    }
}