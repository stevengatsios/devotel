package di.uoa.client.register.thread;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.jackson.JacksonFeature;

import di.uoa.client.register.UserID;
import di.uoa.client.settings.ServerProperties;

/**
 * Handles the registration of a client to the server accumulator.
 * 
 * @author Kostis
 *
 */
public class PCRegister extends Thread {
	public Object lockUnregister = new Object();
	public Object lockRegister = new Object();
	private AtomicBoolean OK;
	UserID userid;
	private Client client;

	public PCRegister() {
		this.userid = UserID.getInstance();
		this.OK = new AtomicBoolean(true);
		client = ClientBuilder.newClient().register(JacksonFeature.class);
	}

	/**
	 * Disconnects from the server. Waits to be notified in
	 * {@link PCRegister#lockDisconnect}
	 * 
	 * @throws InterruptedException
	 */
	public void unregister(String uid) throws ProcessingException {
		WebTarget target = client.target(ServerProperties.getRestPath()
				+ "/credentials/unregister");
		String unregister = target.request().post(
				Entity.entity(uid, MediaType.TEXT_PLAIN), String.class);
		if ("true".equals(unregister)) {
			System.out.println("This pc " + uid + " - disconnected");
		} else {
			System.out.println("Could not disconnect from the server");
		}

	}

	/**
	 * Send User ID to the accumulator.
	 * 
	 * @param id
	 *            the User ID as a string
	 */
	public boolean register(String uid) throws ProcessingException {
		WebTarget target = client.target(ServerProperties.getRestPath()
				+ "/credentials/register");
		String register = target.request(MediaType.TEXT_PLAIN).post(
				Entity.entity(uid, MediaType.TEXT_PLAIN), String.class);
		if ("true".equals(register)) {
			return true;
		}
		return false;

	}

	/**
	 * Starts the registration with the server and then notifies the
	 * {@link PCRegister#lockRegister} that it has completed that operation.
	 */
	public void run() {
		String id = this.userid.getUid();
		if (id != null) {
			try {
				if (!register(id)) {
					System.out.println("Administrator has deleted you");
					OK.set(false);
					return;
				}
			} catch (ProcessingException e) {
				OK.set(false);
				System.err.println("Server not found : " + e.getMessage());
			}
		} else {
			System.out.println("No ID found");
			OK.set(false);
			return;
		}
		synchronized (lockRegister) {
			lockRegister.notify(); // Notifies that it connected
		}
		try {
			synchronized (lockUnregister) {
				lockUnregister.wait();
			}
			if (OK.get()) {
				this.unregister(id);
			}
		} catch (InterruptedException e) {
			return;
		} catch (ProcessingException e) {
			System.err.println("Server not found : " + e.getMessage());
		}
	}

	/**
	 * A boolean to notify parent thread that the registration is complete.
	 * 
	 * @return OK status
	 */
	public boolean isOK() {
		return OK.get();
	}
}
