package di.uoa.server.database.bases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import di.uoa.memory.S_MPSM;
import di.uoa.memory.StatisticalReports;
import di.uoa.server.database.DAO;

public class StatisticsBase {

	/**
	 * Get total frequency for each Malicious IP/Pattern
	 * 
	 * @return {@link ResultSet}
	 */
	public static ResultSet getStatistics() {
		return DAO.executeQuery("SELECT Malicious_pattern, "
				+ "SUM(frequency) as Frequency FROM Statistics "
				+ "GROUP BY Malicious_pattern " + "ORDER BY Frequency");
	}

	/**
	 * Get all records from Statistics Table, ordered by timestamp
	 * 
	 * @return {@link ResultSet}
	 */
	public static ResultSet getHistory() {
		return DAO.executeQuery("SELECT timestamp, Clients_idClient, "
				+ "interface, interfaceIP, Malicious_pattern, frequency "
				+ "FROM Statistics ORDER BY timestamp");
	}

	/**
	 * Get all records according to user id.
	 * 
	 * @param id
	 * @return {@link StatisticalReports}
	 */
	public static StatisticalReports getStatisticsByID(String id) {
		S_MPSM smpsm = new S_MPSM();
		Timestamp timestamp = null;
		ResultSet results = DAO.executeQuery("SELECT interface, interfaceIP, "
				+ "timestamp, Malicious_pattern, "
				+ "SUM(frequency) as Frequency FROM Statistics "
				+ "WHERE Clients_idClient='" + id
				+ "' GROUP BY Malicious_pattern " + "ORDER BY interface");
		try {
			while (results.next()) {
				long freq = results.getLong("Frequency");
				smpsm.updateRegister(results.getString("interface"),
						results.getString("interfaceIP"),
						results.getString("Malicious_pattern"), freq);
				if(timestamp == null) {
					timestamp = results.getTimestamp("timestamp");
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		DAO.closeStatement();
		StatisticalReports report = new StatisticalReports(smpsm, id, timestamp);
		return report;
	}

}
