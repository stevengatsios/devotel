package di.uoa.user.db.tables;

/**
 * Data Class to represents Malicious Table of database
 * 
 * @author Kostis
 *
 */
public class Malicious {
	
	private String pattern;
	private long id;
	
	public Malicious(String pattern, long id) {
		this.setPattern(pattern);
		this.setId(id);
	}
	
	public Malicious() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long l) {
		this.id = l;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String node) {
		this.pattern = node;
	}
	
}
