package di.uoa.server.resources;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import di.uoa.server.database.bases.AndroidsBase;
import di.uoa.server.database.bases.ClientsBase;
import di.uoa.server.memory.*;
import di.uoa.memory.*;

/**
 * Web Service Class for Register and Unregister
 * 
 * @author Kostis
 *
 */
@Path("/credentials")
public class Credentials {

	/**
	 * Interface for the register of Client with nodeID
	 * 
	 * @param nodeID
	 * @return
	 */
	@POST
	@Path("/register")
	@Consumes(MediaType.TEXT_PLAIN)
	public boolean register(String nodeID) {
		ServerMemory memory = ServerMemory.getInstance();
		Node node = memory.getNodeByID(nodeID);
		if (node != null) {
			return false;
		} else {
			memory.add(new Node(nodeID, true));
			if (ClientsBase.register(nodeID)) {
				return true;
			}
			return ClientsBase.changeStatus(nodeID, true);
		}

	}

	/**
	 * Interface for the unregister of Client with nodeID
	 * 
	 * @param nodeID
	 * @return
	 */
	@POST
	@Path("/unregister")
	@Consumes(MediaType.TEXT_PLAIN)
	public boolean unregister(String nodeID) {
		ServerMemory memory = ServerMemory.getInstance();
		Node node = memory.getNodeByID(nodeID);
		if (node != null) {
			memory.remove(node);
			return ClientsBase.changeStatus(nodeID, false);
		}
		return false;
	}

	/**
	 * Mobile phones can register in this path.
	 * 
	 * @param username
	 * @param password
	 * @param nodes
	 * @return
	 */
	@POST
	@Path("/android/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean register(@QueryParam("username") String username,
			@QueryParam("password") String password, AvailableNodes nodes) {		
		try {
			if(!AndroidsBase.Exists(username, password)) { // if user doesn't already exist
				// register him
				if(AndroidsBase.register(username, password)) { 
					// set Android user's Clients
					return AndroidsBase.setClients(username, nodes);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Mobile phones can login in this path.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("android/login")
	@Consumes(MediaType.TEXT_PLAIN)
	public int login(@QueryParam("username") String username, String password) {
		try {
			if(AndroidsBase.Exists(username, password) 
					&& AndroidsBase.changeStatus(username, true)) {
				if(AndroidsBase.isAdmin(username, password)) {
					return 2;
				}
				else return 1;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
	
	/**
	 * Mobile phones can login in this path.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@GET
	@Path("android/login")
	@Produces(MediaType.APPLICATION_JSON)
	public AvailableNodes getNodes(@QueryParam("username") String username, @QueryParam("password") String password) {
		try {
			if(AndroidsBase.Exists(username, password)) {
				return AndroidsBase.getClients(username, password);
			}	
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Mobile phones can logout in this path.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("android/logout")
	@Consumes(MediaType.TEXT_PLAIN)
	public boolean logout(@QueryParam("username") String username, String password) {
		try {
			if(AndroidsBase.Exists(username, password) 
					&& AndroidsBase.changeStatus(username, false)) {
				return true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

}
