package di.uoa.settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;

/**
 * Property Class for opening a properties File(Singleton)
 * 
 * @author Steve, Kostis
 *
 */
public class Settings extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5791858595486055277L;
	static final private String FILENAME = "my.cnf";
	static final private String DEFAULT_FILENAME = "default.cnf";
	static private Settings instance = null;

	/**
	 * Get Property of key
	 * 
	 * @param key
	 * @return String
	 */
	public static String getValue(String key) {
		return getInstance().getProperty(key);
	}

	private static synchronized Settings getInstance() {
		if (instance == null) {
			instance = new Settings();
		}
		return instance;
	}

	/**
	 * Constructor
	 */
	private Settings() {
		super();
		try {
			FileReader reader = new FileReader(new File(FILENAME));
			this.load(reader);
			reader.close();
			System.out.println("Properties loaded successfully from my.cnf");
		} catch (FileNotFoundException e) {
			System.out
					.println("Properties file not found : Creating File my.cnf");
			try {
				this.load(this.getClass().getClassLoader()
						.getResourceAsStream(DEFAULT_FILENAME));
				FileOutputStream stream = new FileOutputStream("my.cnf");
				this.store(new OutputStreamWriter(stream, "utf-8"),
						"auto-generated");
				stream.close();
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		} catch (IOException e2) {
			throw new RuntimeException("IO Error");
		}
	}
}
