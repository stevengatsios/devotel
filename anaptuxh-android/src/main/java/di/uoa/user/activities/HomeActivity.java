package di.uoa.user.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.user.R;
import di.uoa.user.activities.fragments.AdminFragment;
import di.uoa.user.activities.fragments.DashboardFragment;
import di.uoa.user.activities.fragments.FilterInterfaceFragment;
import di.uoa.user.activities.fragments.FilterMaliciousFragment;
import di.uoa.user.activities.fragments.NavigationDrawerFragment;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.tasks.UpdateService;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.Accumulator;
import di.uoa.user.utils.ServerConnector;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v4.widget.DrawerLayout;

/**
 * Fragment managing the behaviors, interactions and presentation of the
 * navigation drawer.
 * 
 * @author Steve
 *
 */
public class HomeActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	
	private NavigationDrawerFragment mNavigationDrawerFragment;
	private Intent mServiceIntent = null;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private UserLogoutTask mLogoutTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getString(R.string.dashboard);
		mServiceIntent = new Intent(Accumulator.getContext(), UpdateService.class);
		Accumulator.getContext().startService(mServiceIntent);
		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}



	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		switch(position) {
		case 0:
			transaction.replace(R.id.container, new DashboardFragment());
			mTitle = getString(R.string.dashboard);
			break;
		case 1:
			transaction.replace(R.id.container, new FilterInterfaceFragment());
			mTitle = getString(R.string.interfaces);
			break;
		case 2:
			transaction.replace(R.id.container, new FilterMaliciousFragment());
			mTitle = getString(R.string.malicious);
			break;
		case 3:
			mLogoutTask = new UserLogoutTask(Account.getUsername(), Account.getPassword());
			mLogoutTask.execute((Void) null);
			break;
		case 4:
			if(Account.getAdmin()) {
				transaction.replace(R.id.container, new AdminFragment());
				mTitle = getString(R.string.admin);
				break;
			}
		default:
			break;
		}
		transaction.commit();
	}

	public void onSectionAttached(String title) {
		mTitle = title;
	}
	
	@SuppressWarnings("deprecation")
	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.global, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (item.getItemId() == R.id.action_refresh) {
			Toast.makeText(this, "EDW NA VALOUME NA KANEI FORCE UPDATE", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		if(item.getItemId() == R.id.greek) {
			Locale locale = new Locale("gr"); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		if(item.getItemId() == R.id.english) {
			Locale locale = new Locale(""); 
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().
            	updateConfiguration(config, 
            		getBaseContext().getResources().getDisplayMetrics());
            restartActivity();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void restartActivity() {
	    Intent intent = getIntent();
	    finish();
	    startActivity(intent);
	}
	
	
	/**
	 * Represents an asynchronous Logout task used to exit from
	 * Application.
	 */
	public class UserLogoutTask extends AsyncTask<Void, Void, Boolean> {

		private final String mUsername;
		private final String mPassword;
		private final Handler handle;

		UserLogoutTask(String username, String password) {
			mUsername = username;
			mPassword = password;
			handle = new Handler(getApplication().getMainLooper());
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			ServerConnector serv = ServerConnector.getInstance();
			List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
			queryParams.add(new BasicNameValuePair("username", mUsername));
			String resp;
			try {
				resp = serv.doPOST("credentials/android/logout", queryParams,
						mPassword, new TypeReference<String>() {
						});
			} catch (final AppException e) {
				handle.post(new Runnable() {
					public void run() {
						Toast.makeText(getApplication(), getString(R.string.no_connection),
								Toast.LENGTH_SHORT).show();
					}
				});
				return false;
			}
			Log.i("LOGOUT", "Server responed with " + resp);
			if(resp.equals("true")) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mLogoutTask = null;

			if (success) {
				Account.setPassword(""); //reset password
				Intent intent = new Intent(getApplication(),SplashLogin.class);
				startActivity(intent);
				Accumulator.getContext().stopService(mServiceIntent);
				finish();

			} 
		}

		@Override
		protected void onCancelled() {
			mLogoutTask = null;
		}
		
	}
}
