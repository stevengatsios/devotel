package uoa.server.test;

import static org.junit.Assert.*;

import org.junit.Test;

import di.uoa.settings.Settings;

public class SettingsTest {

	@Test
	public void test() {
		assertEquals("developer", Settings.getValue("db.user"));
	}
}
