package di.uoa.user.activities.fragments;

import di.uoa.user.R;
import di.uoa.user.activities.fragments.tabs.ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * 
 * Fragment for malicious patterns.
 *
 */
public class FilterMaliciousFragment extends Fragment {
	private FragmentTabHost mTabHost;
	private boolean createdTab = false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(false); //Not sure yet
		mTabHost = new FragmentTabHost(getActivity());
		mTabHost.setup(getActivity(), getChildFragmentManager(),
				R.id.realtabcontent);
		mTabHost.setup(getActivity(), getChildFragmentManager());
		Bundle myDevicesArgument = new Bundle();
		myDevicesArgument.putBoolean(ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.ARGUMENT, true);
		mTabHost.addTab(
				mTabHost.newTabSpec("my_nodes").setIndicator(getActivity().getString(R.string.my_nodes), null),
				ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.class, myDevicesArgument);
		Bundle allDevicesArgument = new Bundle();
		allDevicesArgument.putBoolean(ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.ARGUMENT, false);
		mTabHost.addTab(
				mTabHost.newTabSpec("all_nodes").setIndicator(getActivity().getString(R.string.all_nodes), null),
				ExpandableMaliciousTabFragmentWithShowAllMyDevicesOption.class, allDevicesArgument);
		return mTabHost;
	}

	public void onResume() {
		super.onResume();
		if (!createdTab) {
			createdTab = true;
			mTabHost.setup(getActivity(), getActivity()
					.getSupportFragmentManager());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mTabHost = null;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		//int tab = mTabHost.getCurrentTab();
	}
}
