package di.uoa.user.utils;
import android.app.Application;
import android.content.Context;

/**
 * Singleton Utility Class to get the global Context
 * 
 * @author Steve
 *
 */
public class Accumulator extends Application {
    private static Accumulator instance;

    public static Accumulator getInstance() {
        return instance;
    }

    public static Context getContext(){
        return instance;
        // or return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
