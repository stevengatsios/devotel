package di.uoa.client.network.thread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jnetpcap.*;

import di.uoa.client.network.NetworkInterfaceManager;
import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;
import di.uoa.settings.Settings;

/**
 * Detects Interface Devices. Runs every once in a while to detect changes.
 * 
 * @author Steve, Kostis
 * @see Pcap
 */

public class NetworkInterfaceDetection extends Thread {

	/**
	 * Defines waiting time between each loop.
	 */
	static private int INTERVAL; 
	private MPSM mpsm = null;
	private NetworkInterfaceManager networkInterfaceManager = null;
	/**
	 * Defines the {@link ThreadGroup} that the threads belong.
	 * 
	 * @deprecated
	 */
	private ThreadGroup pool = null;
	private S_MPSM smpsm = null;
	/**
	 * Map of the device name with {@link InterfaceSniffer} thread.
	 */
	private Map<String, InterfaceSniffer> threads = null;

	public NetworkInterfaceDetection(MPSM mpsm, S_MPSM smpsm) {
		super("network detection thread");
		this.mpsm = mpsm;
		this.smpsm = smpsm;
		pool = new ThreadGroup("device sniffers");
		threads = new HashMap<String, InterfaceSniffer>();
		networkInterfaceManager = NetworkInterfaceManager.getInstance();
		INTERVAL = Integer.parseInt(Settings.getValue("detection.update"));
	}

	/**
	 * Takes a list of disabled Interfaces and stops their Threads.
	 */
	private void disabledInterfaces() {
		List<PcapIf> disabledInter;
		disabledInter = networkInterfaceManager.findDisabledDevices();
		if (!disabledInter.isEmpty()) {
			System.out.println("Some Interfaces has been disabled");
			for (PcapIf p : disabledInter) {
				networkInterfaceManager.removeInterface(p);
				InterfaceSniffer t = threads.remove(p.getName());
				if (t.isAlive()) {
					System.out.println("Interrupting " + p.getName());
					t.interrupt();
				}

			}
		}
	}

	/**
	 * 
	 * @return the list of threads hanging from
	 *         {@link NetworkInterfaceDetection}
	 */
	public List<InterfaceSniffer> getThreads() {
		ArrayList<InterfaceSniffer> ret = new ArrayList<InterfaceSniffer>(
				threads.values());
		return ret;
	}

	/**
	 * Takes a list of new Interfaces and for each one starts a new Thread.
	 */
	private void newInterfaces() {
		List<PcapIf> newInter;
		newInter = networkInterfaceManager.findNewDevices();
		if (!newInter.isEmpty()) {
			System.out.println("New Interfaces has been found");
			for (PcapIf p : newInter) {
				networkInterfaceManager.addInterface(p);
				InterfaceSniffer t = new InterfaceSniffer(pool, p, mpsm, smpsm);
				threads.put(p.getName(), t);
				t.start();
			}
		}
	}

	/**
	 * Starts each Thread and checks for updates
	 * {@link NetworkInterfaceDetection#INTERVAL}
	 */
	@Override
	public void run() {
		try {
			while (!this.isInterrupted()) {
				// System.out.println("Checking for network update");
				this.newInterfaces();
				this.disabledInterfaces();
				Thread.sleep(INTERVAL);
			}
		} catch (InterruptedException e) {

		}
		for (InterfaceSniffer t : this.getThreads()) {
			System.out.println("Interrupting " + t.getName());
			if (t.isSniffing()) {
				t.getPcap().breakloop();
			} else
				t.interrupt();
			try {
				t.join();
			} catch (InterruptedException e) {

			}
		}
		System.out.println("Terminated " + this.getName());
	}

}