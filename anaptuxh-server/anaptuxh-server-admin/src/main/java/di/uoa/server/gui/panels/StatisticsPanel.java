package di.uoa.server.gui.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.ResultSetMetaData;

import di.uoa.server.database.bases.StatisticsBase;

/**
 * 
 * @author Kostas
 *
 */
public class StatisticsPanel extends JPanel {
	private static JTable table;
	private ResultSet rs;
	private JScrollPane pane;

	/**
	 * 
	 */
	private static final long serialVersionUID = 3823744139678003561L;

	/**
	 * Constructor - Create a {@link JPanel} that displays Malicious IP/Pattern
	 * Statistics and History using {@link .
	 */
	public StatisticsPanel() {
		setLayout(new BorderLayout());
		JButton stats = new JButton("Statistics");
		JButton history = new JButton("History");
		try {
			rs = StatisticsBase.getStatistics();
			table = new JTable(buildTableModel(rs));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		stats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					rs = StatisticsBase.getStatistics();
					table = new JTable(buildTableModel(rs));
					pane.setViewportView(table);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		history.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					rs = StatisticsBase.getHistory();
					table = new JTable(buildTableModel(rs));
					pane.setViewportView(table);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		pane = new JScrollPane();
		pane.setViewportView(table);
		pane.setPreferredSize(new Dimension(600, 275));
		add(pane, BorderLayout.NORTH);
		add(stats, BorderLayout.WEST);
		add(history, BorderLayout.EAST);
	}

	/**
	 * Take a {@link ResultSet} and create a {@link DefaultTableModel}
	 * 
	 * @param rs
	 * @return {@link DefaultTableModel}
	 * @throws SQLException
	 */
	private DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

		if (rs != null) {
			ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

			// names of columns
			Vector<String> columnNames = new Vector<String>();
			int columnCount = metaData.getColumnCount();
			for (int column = 1; column <= columnCount; column++) {
				columnNames.add(metaData.getColumnName(column));
			}

			// data of the table
			Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			if (rs.last()) {
				do {
					Vector<Object> vector = new Vector<Object>();
					for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
						vector.add(rs.getObject(columnIndex));
					}
					data.add(vector);
				} while (rs.previous());
			}
			return new DefaultTableModel(data, columnNames);
		}
		return null;
	}

}
