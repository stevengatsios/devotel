<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="di.uoa.server.data.MaliciousPOJO"%>
<%@page import="di.uoa.memory.MPSM"%>
<%@page import="di.uoa.server.database.bases.StatisticsBase"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="di.uoa.server.memory.Node"%>
<%@page import="di.uoa.server.database.bases.MaliciousBase"%>
<%@page import="di.uoa.server.database.bases.ClientsBase"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%  %>
<%@include file="includes/header.jsp"%>
<title>Dashboard</title>
</head>
<body>
	<%@include file="includes/navbar.jsp"%>
	<div class="container" style="width: auto;">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-primary">
					<!-- Default panel contents -->
					<div class="panel-heading">Users</div>
					<div class="list-group">
						<%
							for(Node x : ClientsBase.getClients()) {
						%>
						<div class="list-group-item">
							<i class='fa fa-user <%=(x.isOnline()) ? "online" : "offline"%>'></i>
							<%=x.getUserID()%>
						</div>
						<%
							}
						%>
					</div>
				</div>
				<div class="panel panel-primary">
					<!-- Default panel contents -->
					<div class="panel-heading">Patterns</div>
					<ul class="list-group">
						<%
							for(String x : MaliciousBase.getMalicious(true)) {
																																																																										if(!MPSM.isIp(x)) {
						%>
						<li class="list-group-item"><%=x%></li>
						<%
							}
																																																																									}
						%>
					</ul>
				</div>
				<div class="panel panel-primary">
					<!-- Default panel contents -->
					<div class="panel-heading">IPs</div>
					<ul class="list-group">
						<%
							for(String x : MaliciousBase.getMalicious(true)) {
																																																																										if(MPSM.isIp(x)) {
						%>
						<li class="list-group-item"><%=x%></li>
						<%
							}
																																																																									}
						%>
					</ul>
				</div>
			</div>
			<div class="col-md-8">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Pattern</th>
							<th>Frequency</th>
						</tr>
					</thead>
					<tbody>
						<%
							MaliciousPOJO max = null;
							List<MaliciousPOJO> arr = MaliciousPOJO.getMaliciousList();
							if(arr.size() != 0) {
								Collections.sort(arr); Collections.reverse(arr);
								max = Collections.max(arr);
							}
						%>
						<%
							for(MaliciousPOJO pattern : arr) {
						%>
						<tr>
							<td><%=pattern.getPattern()%></td>
							<td>
								<div class="progress">
									<div class="progress-bar" role="progressbar"
										aria-valuenow="<%=pattern.getFrequency()%>" aria-valuemin="0"
										aria-valuemax="<%=max.getFrequency()%>"
										style="width: <%=(double)(pattern.getFrequency())/max.getFrequency() *100%>%;">
										<span class=""><%=pattern.getFrequency()%></span>
									</div>
								</div>
							</td>
						</tr>
						<%
							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<%@include file="includes/scripts.jsp"%>
</body>
</html>