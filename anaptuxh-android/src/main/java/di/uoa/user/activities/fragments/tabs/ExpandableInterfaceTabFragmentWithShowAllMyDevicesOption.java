package di.uoa.user.activities.fragments.tabs;

import java.util.List;



import di.uoa.user.R;
import di.uoa.user.adapters.AbstractExpandableAdapter;
import di.uoa.user.adapters.ExpandableInterfaceAdapter;
import di.uoa.user.adapters.MaliciousAdapter;
import di.uoa.user.db.bases.StatisticBase;
import di.uoa.user.db.tables.Statistic;
import di.uoa.user.utils.Accumulator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.TextView;
/**
 * It accepts a bundle with
 * {@link ExpandableInterfaceTabFragmentWithShowAllMyDevicesOption#ARGUMENT}
 * if its true it will show all devices else just ours.
 */
public class ExpandableInterfaceTabFragmentWithShowAllMyDevicesOption extends Fragment {
	public final static String ARGUMENT = "myDevices";
	AbstractExpandableAdapter listAdapter;
	ExpandableListView expListView;
	boolean myDevices;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(false);
		this.myDevices = getArguments().getBoolean(ARGUMENT);
		View rootView = inflater.inflate(R.layout.exp_list_layout, container,
				false);
		expListView = (ExpandableListView) rootView
				.findViewById((R.id.exp_list_view));
		listAdapter = new ExpandableInterfaceAdapter(getActivity(), myDevices);
		expListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				String client = (String) listAdapter.getGroup(groupPosition);
				String interfaceName = (String) listAdapter.getChild(groupPosition, childPosition);
				InterfaceInfoDialog dialog = new InterfaceInfoDialog(interfaceName, client);
				dialog.show(getFragmentManager(), interfaceName);
				return false;
			}
		});
		expListView.setAdapter(listAdapter);
		return rootView;
	}
	
	/**
	 * 
	 * DialogFragment that shows Interface information.
	 *
	 */
	public class InterfaceInfoDialog extends DialogFragment {
		private String client;
		private String interfaceName;
		private StatisticBase datasource;
		private MaliciousAdapter adapter;
		
		public InterfaceInfoDialog(String interfaceName, String client) {
			this.interfaceName = interfaceName;
			this.client = client;
			datasource = new StatisticBase(Accumulator.getContext());
		}
		
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        LayoutInflater inflater = getActivity().getLayoutInflater();
	        View view = inflater.inflate(R.layout.interface_fragment, null);
	        TextView mClient = (TextView) view.findViewById(R.id.nodeUUID);
	        TextView mIP = (TextView) view.findViewById(R.id.ipText);
	        ListView mList= (ListView) view.findViewById(R.id.malicious_list);
	        datasource.open();
	        List<Statistic> statistics = datasource.
	        							getInterfaceStatistics(client, interfaceName);
	        String ip = datasource.getInterfaceIP(client, interfaceName);
	        datasource.close();
	        adapter = new MaliciousAdapter(getActivity(), statistics);
	        mClient.setText(client);
	        mIP.setText(ip);
	        mList.setAdapter(adapter);	        
	        builder.setMessage(interfaceName)
	               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                   }
	               });
	        builder.setView(view);
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	}
	
	
}