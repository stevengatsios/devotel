package di.uoa.server.memory;

import java.util.ArrayList;

/**
 * Data Class for Memory of Accumulator
 * 
 * @author Kostis
 *
 */
public class ServerMemory extends ArrayList<Node> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ServerMemory instance = null;
	
	public static synchronized ServerMemory getInstance() {
		if (instance == null) {
			instance = new ServerMemory();
		}
		return instance;
	}
	
	/**
	 * Get a node with this nodeID
	 * @param nodeID
	 * @return Node
	 */
	public Node getNodeByID(String nodeID) {
		for (Node node : this) {
			if (node.getUserID().equals(nodeID)) {
				return node;
			}
		}
		return null;
	}
	
	/**
	 * Constructor.
	 * Initialized as empty.
	 */
	private ServerMemory() {
		super();
	}
}
