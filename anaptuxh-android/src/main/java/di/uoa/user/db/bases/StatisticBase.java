package di.uoa.user.db.bases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import di.uoa.memory.S_MPSM.Register;
import di.uoa.user.db.MySqliteHelper;
import di.uoa.user.db.tables.Statistic;

/**
 * Utility Class to retrieve/update data from/to Statistics Table of database
 * 
 * @author Kostas
 *
 */
public class StatisticBase {
	private SQLiteDatabase database;
	private MySqliteHelper dbHelper;
	private String[] allColumns = { MySqliteHelper.COLUMN_ID,
			MySqliteHelper.COLUMN_N_NODE, MySqliteHelper.COLUMN_INTERFACE,
			MySqliteHelper.COLUMN_INTERFACEIP, MySqliteHelper.COLUMN_M_PATTERN,
			MySqliteHelper.COLUMN_FREQ };

	public StatisticBase(Context context) {
		dbHelper = new MySqliteHelper(context);
	}
	
	/**
	 * open database
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	/**
	 * close database
	 */
	public void close() {
		dbHelper.close();
	}

	public Statistic createStatistic(String node, Register register) {
		ContentValues values = new ContentValues();
		values.put(MySqliteHelper.COLUMN_N_NODE, node);
		values.put(MySqliteHelper.COLUMN_INTERFACE, register.getInterfaceName());
		values.put(MySqliteHelper.COLUMN_INTERFACEIP, register.getInterfaceIP());
		values.put(MySqliteHelper.COLUMN_M_PATTERN, register.getMaliciousPattern());
		values.put(MySqliteHelper.COLUMN_FREQ, register.getFrequency());
		long insertId = database.replace(MySqliteHelper.TABLE_STATS, null,
				values);
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				MySqliteHelper.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		Statistic newNode = cursorToStatistic(cursor);
		cursor.close();
		return newNode;
	}
	
	/**
	 * delete statistic {@param stat} from the table
	 * 
	 * @param stat
	 */
	public void deleteStatistic(Statistic stat) {
		long id = stat.getId();
		database.delete(MySqliteHelper.TABLE_STATS, MySqliteHelper.COLUMN_ID
				+ " = " + id, null);
	}
	
	/**
	 * delete all statistics from the table
	 */
	public void deleteAllStatistics() {
		database.delete(MySqliteHelper.TABLE_STATS, null, null);
	}
	
	/**
	 * take all statistics from the table
	 * 
	 * @return
	 */
	public List<Statistic> getAllStatistics() {
		List<Statistic> statistics = new ArrayList<Statistic>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				null, null, null, null, MySqliteHelper.COLUMN_FREQ);

		cursor.moveToLast();
		while (!cursor.isBeforeFirst()) {
			Statistic stat = cursorToStatistic(cursor);
			statistics.add(stat);
			cursor.moveToPrevious();
		}
		// make sure to close the cursor
		cursor.close();
		return statistics;
	}
	
	/**
	 * take all statistics of {@param nodes} as HashMap of node UUID, List of statistics
	 * 
	 * @param nodes
	 * @return
	 */
	public HashMap<String,List<Statistic>> getAllStatisticsAsMap(List<String> nodes) {
		if(nodes == null) {
			return null;
		}
		HashMap<String,List<Statistic>> map = new HashMap<String,List<Statistic>>();
		for(String node : nodes) {
			List<Statistic> statistics = new ArrayList<Statistic>();
			String[] columns = {MySqliteHelper.COLUMN_M_PATTERN,
					"SUM(" + MySqliteHelper.COLUMN_FREQ +")"};
			Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, columns,
					MySqliteHelper.COLUMN_N_NODE + "='" + node + "'", null, columns[0], null, columns[1]);
	
			cursor.moveToLast();
			while (!cursor.isBeforeFirst()) {
				Statistic stat = cursorToHalfStatistic(cursor);
				statistics.add(stat);
				cursor.moveToPrevious();
			}
			// make sure to close the cursor
			cursor.close();
			map.put(node, statistics);
		}
		return map;
	}
	
	/**
	 * take all interfaces of {@param nodes} as HashMap of node UUID, List of interfaces
	 * 
	 * @param nodes
	 * @return
	 */
	public HashMap<String,List<String>> getAllInterfacesAsMap(List<String> nodes) {
		if(nodes == null) {
			return null;
		}
		HashMap<String,List<String>> map = new HashMap<String,List<String>>();
		for(String node : nodes) {
			List<String> interfaces = new ArrayList<String>();
			Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
					MySqliteHelper.COLUMN_N_NODE + "='" + node + "'", null, null, null, null);
	
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Statistic stat = cursorToStatistic(cursor);
				if(!interfaces.contains(stat.getInter())) {
					interfaces.add(stat.getInter());
				}
				cursor.moveToNext();
			}
			// make sure to close the cursor
			cursor.close();
			map.put(node, interfaces);
		}
		return map;
	}
	
	/**
	 * take IP of device {@param device} of user with node UUID {@param node}
	 * 
	 * @param node
	 * @param device
	 * @return
	 */
	public String getInterfaceIP(String node, String device) {
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				MySqliteHelper.COLUMN_N_NODE + " = '" + node + "'", null, null, null, null);
		String ip = "";
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Statistic stat = cursorToStatistic(cursor);
			if(stat.getInter().equals(device)) {
				ip = stat.getInterfaceIP();
				break;
			}
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return ip;
	}
	
	/**
	 * Get all statistics of a specific interface
	 * 
	 * @param node
	 * @param device
	 * @return
	 */
	public List<Statistic> getInterfaceStatistics(String node, String device) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				MySqliteHelper.COLUMN_N_NODE + " = '" + node + "'",
				null, null, null, MySqliteHelper.COLUMN_FREQ);

		cursor.moveToLast();
		while (!cursor.isBeforeFirst()) {
			Statistic stat = cursorToStatistic(cursor);
			if(stat.getInter().equals(device)) {
				statistics.add(stat);
			}
			cursor.moveToPrevious();
		}
		// make sure to close the cursor
		cursor.close();
		return statistics;
	}
	
	/**
	 * Filter them by parameter Node
	 * @param node
	 * @return
	 */
	public List<Statistic> getStatistics(String node) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				MySqliteHelper.COLUMN_N_NODE + " = '" + node + "'",
				null, null, null, MySqliteHelper.COLUMN_FREQ);

		cursor.moveToLast();
		while (!cursor.isBeforeFirst()) {
			Statistic stat = cursorToStatistic(cursor);
			statistics.add(stat);
			cursor.moveToPrevious();
		}
		// make sure to close the cursor
		cursor.close();
		return statistics;
	}
	
	/**
	 * Get all interfaces of user with UUID {@param}
	 * 
	 * @param node
	 * @return
	 */
	public List<String> getInterfaces(String node) {
		List<String> interfaces = new ArrayList<String>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_STATS, allColumns,
				MySqliteHelper.COLUMN_N_NODE + " = '" + node + "'", null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Statistic stat = cursorToStatistic(cursor);
			interfaces.add(stat.getInter());
			cursor.moveToNext();
		}
		cursor.close();
		return interfaces;
	}
	
	/**
	 * Convert result to a statistic Malicious-Frequency
	 * 
	 * @param cursor
	 * @return
	 */
	private Statistic cursorToHalfStatistic(Cursor cursor) {
		Statistic stat = new Statistic();
		stat.setMalicious(cursor.getString(0));
		stat.setFrequency(cursor.getLong(1));
		return stat;
	}
	
	/**
	 * Convert result to a Statistic
	 * 
	 * @param cursor
	 * @return
	 */
	private Statistic cursorToStatistic(Cursor cursor) {
		Statistic stat = new Statistic();
		stat.setId(cursor.getLong(0));
		stat.setNode(cursor.getString(1));
		stat.setInter(cursor.getString(2));
		stat.setInterfaceIP(cursor.getString(3));
		stat.setMalicious(cursor.getString(4));
		stat.setFrequency(cursor.getLong(5));
		return stat;
	}
	
}
