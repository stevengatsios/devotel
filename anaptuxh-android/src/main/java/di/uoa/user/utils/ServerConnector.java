package di.uoa.user.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.user.R;
import di.uoa.user.exceptions.AppException;
import android.util.Log;

/**
 * Singleton class to manage the connection with the server
 * @author Steve
 *
 */
public class ServerConnector {
	private HttpClient client;
	static private ServerConnector instance = null;
	static private int TIMEOUT = 10000;
	static public String PATH = "http://"
			+ Accumulator.getContext().getString(R.string.server_ip) + ":"
			+ Accumulator.getContext().getString(R.string.server_port) + "/"
			+ Accumulator.getContext().getString(R.string.server_path) + "/"
			+ Accumulator.getContext().getString(R.string.server_rest) + "/";

	private ServerConnector() {
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, TIMEOUT);
		HttpConnectionParams.setSoTimeout(params, TIMEOUT);
		client = new DefaultHttpClient(params);
		Log.i("Client", "Client points at " + PATH);
	}

	static public synchronized ServerConnector getInstance() {
		if (instance == null) {
			instance = new ServerConnector();
		}
		return instance;
	}

	/**
	 * Do a GET request to Server
	 * 
	 * @param relativePath to the api
	 * @param params an array of strings in a form param=value
	 * @param returnType the class to be deserialized to
	 * @return an Object of that class
	 * @throws AppException 
	 */
	public <T> T doGET(String relativePath, List<NameValuePair> params, TypeReference<T> returnType) throws AppException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.AUTO_DETECT_FIELDS, true);
		HttpGet getUrl = null;
		String url = new String(PATH) + relativePath;
		// add ? for params
		url += "?" + URLEncodedUtils.format(params, "utf-8");
		getUrl = new HttpGet(url);
		HttpResponse response = null;
		try {
			Log.i("GET", "@" + getUrl.getURI().toString());
			response = this.client.execute(getUrl);
			if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new AppException(Accumulator.getContext().getString(R.string.error_server_message));
			}
			String responseText = readEntity(response.getEntity());
			Log.i("GET Return",responseText);
			return mapper.readValue(responseText, returnType);
		} catch(ClientProtocolException ep) {
			throw new AppException(ep.getLocalizedMessage());
		} catch (IOException e) {
			Log.e("LOGIN", e.getMessage());
			return null;
		}
	}
	
	/**
	 * Do a POST request to Server
	 * 
	 * @param relativePath to the api
	 * @param params an array of strings in a form param=value
	 * @param returnType the class to be deserialized to
	 * @param object the object to be sent via post
	 * @return
	 * @throws AppException 
	 */
	public <T> T doPOST(String relativePath, List<NameValuePair> params, Object object ,TypeReference<T> returnType) throws AppException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		HttpPost postUrl = null;
		String url = new String(PATH) + relativePath;
		// add ? for params
		url += "?" + URLEncodedUtils.format(params, "utf-8");
		postUrl = new HttpPost(url);
		postUrl.setHeader("Content-type", "application/json");
		StringWriter stringEmp = new StringWriter();
		
		try {
			mapper.writeValue(stringEmp, object);
			postUrl.setEntity(new StringEntity(stringEmp.toString()));
			HttpResponse response = null;
			Log.i("POST", "@" + postUrl.getURI().toString());
			Log.i("POST Object",stringEmp.toString());
			response = this.client.execute(postUrl);
			int returnCode = response.getStatusLine().getStatusCode(); 
			if( returnCode!= HttpStatus.SC_OK && returnCode != HttpStatus.SC_NO_CONTENT) {
				throw new AppException(Accumulator.getContext().getString(R.string.error_server_message));
			}
			if(returnType == null) {
				return null;
			} else {
				String responseText = readEntity(response.getEntity());
				return mapper.readValue(responseText, returnType);
			}
		} catch(IOException ep) {
			throw new AppException(ep.getLocalizedMessage());
		}
	}

	/**
	 * Overloading {@link ServerConnector#doPOST(String, List, Object, Class)} for Strings
	 * @param relativePath
	 * @param params
	 * @param object string
	 * @param returnType class to be deJSONed
	 * @return response of the server
	 * @throws AppException
	 */
	public <T> T doPOST(String relativePath, List<NameValuePair> params, String object ,TypeReference<T> returnType) throws AppException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		HttpPost postUrl = null;
		String url = new String(PATH) + relativePath;
		// add ? for params
		url += "?" + URLEncodedUtils.format(params, "utf-8");
		postUrl = new HttpPost(url);
		try {
			postUrl.setEntity(new StringEntity(object));
			HttpResponse response = null;
			Log.i("POST", "@" + postUrl.getURI().toString());
			Log.i("POST Object",object);
			response = this.client.execute(postUrl);
			int returnCode = response.getStatusLine().getStatusCode(); 
			if( returnCode!= HttpStatus.SC_OK && returnCode != HttpStatus.SC_NO_CONTENT) {
				throw new AppException(Accumulator.getContext().getString(R.string.error_server_message));
			}
			if(returnType == null) {
				return null;
			} else {
				String responseText = readEntity(response.getEntity());
				return mapper.readValue(responseText, returnType);
			}
		} catch(IOException ep) {
			throw new AppException(ep.getLocalizedMessage());
		}
	}
	
	/**
	 * Only for JSON, text responses.
	 * @param entity sent from the server
	 * @return the entity as a string
	 */
	private String readEntity(HttpEntity entity) {
		if (entity == null) {
			return "";
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					entity.getContent(), "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + " ");
			}
			return sb.toString();
		} catch (IOException e) {
			Log.e("Entity Reader", e.getMessage());
		}
		return null;
	}
}
