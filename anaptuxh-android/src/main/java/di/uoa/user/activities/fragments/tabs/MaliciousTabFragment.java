package di.uoa.user.activities.fragments.tabs;

import di.uoa.memory.MPSM;
import di.uoa.user.R;
import di.uoa.user.db.bases.MaliciousBase;
import di.uoa.user.tasks.PatternService;
import di.uoa.user.utils.Accumulator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 
 * Fragment that creates a malicious tab.
 *
 */
public class MaliciousTabFragment extends DialogFragment {
	public final static String BUNDLE_OPTION = "TYPE";
	public final static String OPTION_IP = "IP";
	public final static String OPTION_PATTERN = "PATTERN";
	public ArrayAdapter<String> adapter;
	private MaliciousBase source;
	private static boolean show = false;
	private ListView rootView;
	private String type;
	private ServerUpdateReceiver mMessageReceiver;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMessageReceiver = new ServerUpdateReceiver();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter(ServerUpdateReceiver.BROADCAST_FILTER));
		setHasOptionsMenu(true);
		rootView = (ListView) inflater.inflate(R.layout.fragment_dashboard, container, false);
		type = getArguments().getString(BUNDLE_OPTION);
		PatternLoader loader = new PatternLoader();
		loader.execute((Void)null);
		
		return rootView;
	}

	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        //inflater.inflate(R.menu.pattern, menu);
    }
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.pattern_settings) {
			if(!show) {
				PatternAddDialog dialog = new PatternAddDialog();
				dialog.show(this.getFragmentManager(),"pattern");
				show = true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	

	private class PatternLoader extends AsyncTask<Void, Void, ArrayAdapter<String>> {
		public PatternLoader() {
			source = new MaliciousBase(getActivity());
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
			source.open();
		}

		@Override
		protected ArrayAdapter<String> doInBackground(Void... params) {
			for(String malicious : source.getAllMalicious()) {
				boolean ipFlag = type.equals(OPTION_IP);
				if(ipFlag && MPSM.isIp(malicious)) {
					adapter.add(malicious);
				} else if(!ipFlag && !MPSM.isIp(malicious)){
					adapter.add(malicious);
				}
			}
			return adapter;
		}
		
		@Override
		protected void onPostExecute(ArrayAdapter<String> result) {
			super.onPostExecute(result);
			rootView.setAdapter(adapter);
			source.close();
		}
	}

	@SuppressLint("InflateParams")
	public class PatternAddDialog extends DialogFragment {
		
		private MaliciousBase datasource;
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			datasource = new MaliciousBase(getActivity());
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();
			final View dialogView = inflater.inflate(R.layout.popup_pattern, null);
			builder.setView(dialogView)
			// Add action buttons
			.setPositiveButton(R.string.popup_text_accept, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					EditText patternView = (EditText) dialogView.findViewById(R.id.popup_textfield);
					String pattern = patternView.getText().toString();
					datasource.open();
					if(!datasource.exists(pattern)) {
						datasource.createMalicious(pattern);
						Intent mServiceIntent = new Intent(getActivity(), PatternService.class);
						mServiceIntent.putExtra(PatternService.PARAM_INSERT_PATTERN, pattern);
						getActivity().startService(mServiceIntent);						
					}
					else {
						Toast.makeText(Accumulator.getContext(),
								pattern + Accumulator.getContext().getString(R.string.pattern_exists),
								Toast.LENGTH_SHORT).show();
					}
					datasource.close();
					show = false;
				}
			})
			.setNegativeButton(R.string.popup_text_decline, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					show = false;
				}
			});      
			return builder.create();
		}
	}

	public class ServerUpdateReceiver extends BroadcastReceiver {
		 public static final String BROADCAST_FILTER =   
			      "di.uoa.user.SERVER_UPDATED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getStringExtra(PatternService.BROADCAST_ACTION);
			String data = intent.getStringExtra(PatternService.BROADCAST_MESSAGE);
			//Format text and insert it int the view adapter
			if(action.equals(PatternService.PARAM_INSERT_PATTERN) && type.equals(OPTION_PATTERN)) {
				adapter.add(data);
			} else if (action.equals(PatternService.PARAM_INSERT_PATTERN) && type.equals(OPTION_IP)) {
				adapter.add(data);
			}
			adapter.notifyDataSetChanged();
		}
	}
}
