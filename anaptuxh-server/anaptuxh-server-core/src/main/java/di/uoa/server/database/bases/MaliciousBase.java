package di.uoa.server.database.bases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import di.uoa.memory.MPSM;
import di.uoa.server.database.DAO;

public class MaliciousBase {
	
	/**
	 * Get All valid or invalid Malicious
	 * 
	 * @param valid
	 * @return List
	 */
	public static List<String> getMalicious(boolean valid) {
		List<String> ret = new ArrayList<String>();
		ResultSet resultset;
		resultset = DAO.executeQuery("SELECT pattern FROM Malicious "
				+ "WHERE valid=" + valid);
		try {
			while (resultset.next()) {
				String pattern = resultset.getString("pattern");
				ret.add(pattern);
			}
		} catch (SQLException e) {
			System.err.println(e.getSQLState());
		}
		DAO.closeStatement();
		return ret;
	}
	
	/**
	 * Get All valid or invalid Malicious IPs
	 * 
	 * @param valid
	 * @return List
	 */
	public static List<String> getMaliciousIPs(boolean valid) {
		List<String> ret = new ArrayList<String>();
		ResultSet resultset;
		resultset = DAO.executeQuery("SELECT pattern FROM Malicious " 
				+ "WHERE valid=" + valid);
		try {
			while (resultset.next()) {
				String pattern = resultset.getString("pattern");
				if(MPSM.isIp(pattern)) {
					ret.add(pattern);
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getSQLState());
		}
		DAO.closeStatement();
		return ret;
	}
	
	/**
	 * Get All valid or invalid Malicious Patterns
	 * 
	 * @param valid
	 * @return List
	 */
	public static List<String> getMaliciousPatterns(boolean valid) {
		List<String> ret = new ArrayList<String>();
		ResultSet resultset;
		resultset = DAO.executeQuery("SELECT pattern FROM Malicious "
				+ "WHERE valid=" + valid);
		try {
			while (resultset.next()) {
				String pattern = resultset.getString("pattern");
				if(!MPSM.isIp(pattern)) {
					ret.add(pattern);
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getSQLState());
		}
		DAO.closeStatement();
		return ret;
	}
	
	public static void insertMalicious(String malicious) {
		if(malicious != null) {
			List<String> list = new ArrayList<String>(Arrays.asList(malicious.split(",")));
			for(String pattern : list) {
				DAO.executeUpdate("INSERT INTO Malicious VALUES('" + pattern + "', true)");
			}
		}
	}
}
