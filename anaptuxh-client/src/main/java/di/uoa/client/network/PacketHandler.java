package di.uoa.client.network;

import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;

import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;

/**
 * implementation of {@link PcapPacketHandler} that extends a
 * {@link PacketAnalyser} class to handle each packet.
 * 
 * @author Kostas
 *
 */
public class PacketHandler extends PacketAnalyser implements
		PcapPacketHandler<String> {
	MPSM mpsm = null;
	S_MPSM smpsm = null;

	public PacketHandler(MPSM mpsm, S_MPSM smpsm, PcapIf device) {
		super(mpsm, smpsm, device);
		this.mpsm = mpsm;
		this.smpsm = smpsm;
	}

	@Override
	public void nextPacket(PcapPacket nextPacket, String arg1) {
		this.analyse(nextPacket);
	};

}