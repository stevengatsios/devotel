package di.uoa.client.network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapAddr;
import org.jnetpcap.PcapIf;

/**
 * Utility class to handle the network interfaces.
 * 
 * @version Singleton
 * @authors Steve, Kostis, Kostas
 *
 */
public final class NetworkInterfaceManager {

	private static NetworkInterfaceManager instance = null;

	/**
	 * Wrapper for jnetpcap function to get all NICs.
	 * 
	 * @return List of {@link PcapIf}
	 */
	@SuppressWarnings("deprecation")
	public static synchronized List<PcapIf> findNetworkDevices() {
		int r;
		List<PcapIf> alldevs = new ArrayList<PcapIf>();
		List<PcapIf> netdevs = new ArrayList<PcapIf>();
		StringBuilder errbuf = new StringBuilder();
		r = Pcap.findAllDevs(alldevs, errbuf); // take all interfaces
		if (!(r == Pcap.NOT_OK || alldevs.isEmpty())) {
			/*
			 * keep only network interfaces
			 */
			for (PcapIf dev : alldevs) {
				if (!dev.getAddresses().isEmpty()) {
					netdevs.add(dev);
				}
			}
		}
		return netdevs;
	}

	/**
	 * 
	 * @param network
	 * @return IP of network as a String
	 */

	public static String getInetAddress(PcapIf network) {
		if (!network.getAddresses().isEmpty()) {
			for (PcapAddr addresses : network.getAddresses()) {
				String ip = addresses.getAddr().toString();
				if (ip.contains("INET")) {
					return ip;
				}
			}
		}
		return null;
	}

	/**
	 * Singleton pattern.
	 * 
	 * @return a class of {@link NetworkInterfaceManager}
	 */
	static public synchronized NetworkInterfaceManager getInstance() {
		if (NetworkInterfaceManager.instance == null) {
			NetworkInterfaceManager.instance = new NetworkInterfaceManager();
		}
		return NetworkInterfaceManager.instance;
	}

	private Map<String, PcapIf> networkInterfaces = null;

	private NetworkInterfaceManager() {
		networkInterfaces = new HashMap<String, PcapIf>();
	}

	/**
	 * Add interface to the list.
	 * 
	 * @param inter
	 */
	public void addInterface(PcapIf inter) {
		networkInterfaces.put(inter.getName(), inter);
	}

	/**
	 * Checks if there are disabled interfaces and return them
	 * 
	 * @return List of {@link PcapIf}
	 */
	public List<PcapIf> findDisabledDevices() {
		List<PcapIf> current = findNetworkDevices();
		List<PcapIf> last = new ArrayList<PcapIf>(this.getInterfaces());
		List<PcapIf> clone = new ArrayList<PcapIf>(last);
		/*
		 * keep in "last" list the devices which are not in the current list
		 */
		for (PcapIf x : clone) {
			for (PcapIf y : current) {
				if (y.getName().equals(x.getName())) {
					last.remove(x);
				}
			}
		}
		return last;
	}

	/**
	 * Checks if there are new interfaces and return them
	 * 
	 * @return List of{@link PcapIf}
	 * 
	 */
	public List<PcapIf> findNewDevices() {
		List<PcapIf> ret = findNetworkDevices();
		List<PcapIf> clone = new ArrayList<PcapIf>(ret);
		for (PcapIf x : clone) {
			if (networkInterfaces.containsKey(x.getName())) {
				ret.remove(x);
			}
		}
		return ret;
	}

	/**
	 * Getter for interfaces.
	 * 
	 * @return List of {@link PcapIf}
	 */
	public List<PcapIf> getInterfaces() {
		return new ArrayList<PcapIf>(networkInterfaces.values());
	}

	/**
	 * Remove interface from the list.
	 * 
	 * @param inter
	 */
	public void removeInterface(PcapIf inter) {
		networkInterfaces.remove(inter.getName());
	}

	/**
	 * Visually report interfaces
	 */
	public void reportInterfaces() {
		int counter = 0;
		for (PcapIf x : this.getInterfaces()) {
			System.out.printf("[%d] %s %s\n", counter++, x.getDescription(),
					x.getName());
		}
	}

}
