package di.uoa.server.database.bases;

import java.sql.ResultSet;
import java.sql.SQLException;

import di.uoa.server.database.DAO;
import di.uoa.memory.AvailableNodes;

public class AndroidsBase {
	
	/**
	 * Check if android user with {@param username} exists in database
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public static boolean Exists(String username, String password) throws SQLException {
		ResultSet rs;
		boolean ret;
		rs = DAO.executeQuery("SELECT * FROM Androids "
				+ "WHERE username='" + username + "' AND password='" + password + "'");
		ret = rs.next();
		DAO.closeStatement();
		return ret;
	}
	
	/**
	 * Change status of Android user with {@param username}
	 * 
	 * @param username
	 * @param login
	 * @return
	 */
	public static boolean changeStatus(String username, boolean login) {
		return DAO.executeUpdate("UPDATE Androids SET login=" + login
				+ " WHERE username='" + username + "'");
	}
	
	/**
	 * Register Android user with {@param username} and {@param password} to database
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static boolean register(String username, String password) {
		return DAO.executeUpdate("INSERT INTO Androids VALUES('"
				+ username +"', '" + password + "', false, false)");
	}
	
	/**
	 * Set Clients of Android user with {@param username}
	 * 
	 * @param username
	 * @param nodes
	 * @return
	 */
	public static boolean setClients(String username, AvailableNodes nodes) {
		if(nodes != null) {
			for(String node : nodes) {
				try {
					if(ClientsBase.Exists(node)) {
						if(!DAO.executeUpdate("INSERT INTO Androids_has_Clients "
								+ "VALUES('" + username + "','" + node
								+ "')")) { 
							return false;
						}
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Check if Android user with {@param username} and {@param password} is Admin
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static boolean isAdmin(String username, String password) {
		ResultSet rs;
		boolean ret = false;
		rs = DAO.executeQuery("SELECT * FROM Androids "
				+ "WHERE username='" + username + "' AND password='"
				+ password + "'");
		try {
			if(rs.next()) {
				if(rs.getBoolean("admin")) {
					ret = true; //if he is administrator
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		DAO.closeStatement();
		return ret; // if he is not registered
	}
	
	//TODO SVISIMO
	/**
	 * Get all Clients of Android user with {@param username} or all Clients if user is Administrator
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public static AvailableNodes getClients(String username, String password) throws SQLException {
		AvailableNodes ret = null;
		ResultSet rs;
		rs = DAO.executeQuery("SELECT admin FROM Androids WHERE "
				+ "username='" + username + "' AND password='" + password + "'");
		if(rs.next()) {
			ret = new AvailableNodes();
			if(rs.getBoolean("admin")) {
				rs = DAO.executeQuery("SELECT idClient FROM Clients");
				while(rs.next()) {
					String id = rs.getString("idClient");
					ret.add(id);
				}
			}
			else {
				rs.close();
				rs = DAO.executeQuery("SELECT Clients_idClient FROM Androids_has_Clients "
						+ "WHERE Androids_username='" + username + "'");
				while(rs.next()) {
					String id = rs.getString("Clients_idClient");
					ret.add(id);
				}
			}
		}
		DAO.closeStatement();
		return ret;
	}
	
	public static void removeNode(String node) {
		DAO.executeUpdate("DELETE FROM Statistics WHERE Clients_idClient='" + node + "'");
		DAO.executeUpdate("DELETE FROM Clients WHERE idClient='" + node + "'");
		DAO.closeStatement();
	}
	
}
