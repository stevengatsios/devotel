package di.uoa.user.adapters;

import di.uoa.user.R;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.DrawerMenu;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Helper class to Draw Menu
 * 
 * @author Steve
 *
 */
public class DrawerAdapter extends ArrayAdapter<DrawerMenu> {

	private boolean isAdmin;
	
	public DrawerAdapter(Context context, int resource, int textViewResourceId) {
		super(context, resource, textViewResourceId);
		this.isAdmin = Account.getAdmin();
		DrawerMenu dashboard = new DrawerMenu(R.string.dashboard, R.drawable.ic_home, Color.BLACK);
		DrawerMenu interfaces = new DrawerMenu(R.string.interfaces, R.drawable.ic_uuid, Color.BLACK);
		DrawerMenu malicious = new DrawerMenu(R.string.malicious, R.drawable.ic_pattern, Color.BLACK);
		DrawerMenu logout = new DrawerMenu(R.string.logout, R.drawable.ic_logout, Color.BLACK);
		DrawerMenu admin = new DrawerMenu(R.string.admin, R.drawable.ic_admin_tools, Color.BLACK);
		super.add(dashboard);
		super.add(interfaces);
		super.add(malicious);
		super.add(logout);
		if(isAdmin) {
			super.add(admin);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DrawerMenu menu = getItem(position);
		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.navigation_item, parent, false);
		}
		TextView menuText = (TextView) convertView.findViewById(R.id.title);
		ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
		menuText.setText(menu.getTitle());
		menuText.setTextColor(menu.getColor());
		icon.setImageResource(menu.getIconId());
		return convertView;
	}

}
