package di.uoa.client.network;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;
import di.uoa.settings.Settings;

/**
 * Analyzes the content of a TCP or UDP packet.
 * 
 * @author Kostas
 */
public class PacketAnalyser {

	/**
	 * Function that counts the number of appearances of a pattern inside the
	 * payload of a packet.
	 * 
	 * @param x
	 * @param payload
	 * @return count
	 */
	public static int count(Pattern x, String payload) {
		int count = 0;
		Matcher match = x.matcher(payload);
		while (match.find()) {
			count++;
		}
		return count;
	}

	private PcapIf device = null;
	private Ip4 ip = null;
	private MPSM mpsm = null;
	private Payload payload = null;
	private byte[] sIP = null, dIP = null;
	private S_MPSM smpsm = null;
	private String sourceIP, destinationIP;
	private Tcp tcp = null;

	private Udp udp = null;

	public PacketAnalyser(MPSM mpsm, S_MPSM smpsm, PcapIf device) {
		this.mpsm = mpsm;
		this.smpsm = smpsm;
		this.device = device;
		ip = new Ip4();
		tcp = new Tcp();
		udp = new Udp();
		payload = new Payload();
		sIP = new byte[4];
		dIP = new byte[4];
	}

	/**
	 * Analyzes a packet and detects malicious source or destination IPs and
	 * malicious string patterns inside the payload of the packet according to
	 * MPSM.
	 * 
	 * @param packet
	 */
	public void analyse(PcapPacket packet) {

		if (packet.hasHeader(ip)
				&& (packet.hasHeader(tcp) || packet.hasHeader(udp))) {
			// get source and destination IPs from packet
			sIP = packet.getHeader(ip).source();
			dIP = packet.getHeader(ip).destination();
			sourceIP = FormatUtils.ip(sIP);
			destinationIP = FormatUtils.ip(dIP);
			String interfaceIp = NetworkInterfaceManager.getInetAddress(device);
			// check mpsm if source or destination IPs are malicious
			if (mpsm.getMaliciousIPs().contains(sourceIP)) {
				smpsm.updateRegister(device.getName(), interfaceIp, sourceIP, 1);
			}
			if (mpsm.getMaliciousIPs().contains(destinationIP)) {
				smpsm.updateRegister(device.getName(), interfaceIp,
						destinationIP, 1);
			}
			// check payload for malicious string patterns
			if (packet.hasHeader(payload)) {
				//if the destination is the server ignore packet.
				if (sourceIP.contains(Settings.getValue("server.ip"))
						|| destinationIP.contains(Settings
								.getValue("server.ip"))) {
					return;
				}
				byte[] ba = packet.getHeader(payload).getByteArray(0,
						payload.size());
				String payloadData = new String(ba);
				// get malicious patterns from MPSM
				List<Pattern> patterns = mpsm.getMaliciousPatterns();
				for (Pattern x : patterns) {
					int count = PacketAnalyser.count(x, payloadData);
					if (count != 0) {
						smpsm.updateRegister(device.getName(), interfaceIp,
								x.toString(), count);
					}
				}
			}
		}
	}
}
