package di.uoa.user.activities.fragments.tabs;

import java.util.ArrayList;
import java.util.List;

import di.uoa.user.R;
import di.uoa.user.adapters.DashboardAdapter;
import di.uoa.user.db.bases.StatisticBase;
import di.uoa.user.db.tables.Statistic;
import di.uoa.user.utils.Account;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * It accepts a bundle with {@link ExpendableDashboardTab#ARGUMENT} if its true
 * it will show all devices else just ours.
 */
public class DashboardTab extends Fragment {

	public final static String ARGUMENT = "myDevices";
	DashboardAdapter listAdapter;
	boolean myDevices;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(false);
		this.myDevices = getArguments().getBoolean(ARGUMENT);
		ListView rootView = (ListView) inflater.inflate(
				R.layout.fragment_dashboard, container, false);
		StatisticBase datasource = new StatisticBase(getActivity());
		datasource.open();
		List<Statistic> list = new ArrayList<Statistic>();
		if (!myDevices) {
			list.addAll(datasource.getAllStatistics());
		} else {

			List<String> nodes = new ArrayList<String>(
					Account.getAvailableNodes());
			for (String node : nodes) {
				list.addAll(datasource.getStatistics(node));
			}
		}
		datasource.close();
		rootView.setAdapter(new DashboardAdapter(getActivity(), list));
		return rootView;
	}
}
