package di.uoa.user.adapters;
import java.util.List;

import di.uoa.user.R;
import di.uoa.user.db.tables.Statistic;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adapter to create a list of Statistics 
 * 
 * @author Steve
 *
 */
public class DashboardAdapter extends ArrayAdapter<Statistic> {
	
	public DashboardAdapter(Context context) {
		super(context, 0);
	}
	


	public DashboardAdapter(Context context, List<Statistic> objects) {
		super(context, 0, objects);
	}



	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // Get the data item for this position
       Statistic report = getItem(position);    
       // Check if an existing view is being reused, otherwise inflate the view
       if (convertView == null) {
          convertView = LayoutInflater.from(getContext()).inflate(R.layout.dashboard_list_layout, parent, false);
       }
       // Lookup view for data population
       TextView patternText = (TextView) convertView.findViewById(R.id.patternText);
       TextView interfaceText = (TextView) convertView.findViewById(R.id.interfaceText);
       TextView frequencyText = (TextView) convertView.findViewById(R.id.frequencyText);
       TextView ipText = (TextView) convertView.findViewById(R.id.ipText);
       TextView nodeId = (TextView) convertView.findViewById(R.id.nodeUUID);
       float x = (float) report.getFrequency(); 
       float SfromHSV = (float) ((report.getFrequency() != 0 ) ? (80.0/Math.PI)*Math.atan(Math.log(x)/250.0) : 0.0);
       float[] hsv = { 275, (float) SfromHSV, 100 };
       convertView.setBackgroundColor(Color.HSVToColor(hsv));
       // Populate the data into the template view using the data object
       patternText.setText(report.getMalicious());
       frequencyText.setText(report.getFrequency() + "");
       interfaceText.setText(report.getInter());
       ipText.setText(report.getInterfaceIP());
       nodeId.setText(report.getNode());
       // Return the completed view to render on screen
       return convertView;
   }
	
}
