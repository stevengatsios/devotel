package di.uoa.memory;

/**
 * 
 * Data Class to hold malicious patterns who added or deleted from database
 * 
 * @author Steve
 *
 */
public class MaliciousPatterns {

	private MPSM added;
	private MPSM deleted;
	
	/**
	 * Constructor
	 */
	public MaliciousPatterns() {
		added = new MPSM();
		deleted = new MPSM();
	}

	public MPSM getAdded() {
		return added;
	}

	public void setAdded(MPSM added) {
		this.added = added;
	}

	public MPSM getDeleted() {
		return deleted;
	}

	public void setDeleted(MPSM deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("-----ADDED   :-------\n");
		ret.append(added.toString() + "\n");
		ret.append("-----DELETED :-------\n");
		ret.append(deleted.toString());
		return ret.toString();
	}

}
