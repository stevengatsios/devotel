package uoa.client.thread;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import di.uoa.client.network.thread.NetworkInterfaceDetection;
import di.uoa.client.thread.MemorySynchronizer;
import di.uoa.memory.MPSM;
import di.uoa.memory.S_MPSM;

public class NetworkInterfaceDetectionTest {

	MPSM mpsm;
	S_MPSM smpsm;

	@Before
	public void init() {
		mpsm = new MPSM();
		smpsm = new S_MPSM();
	}

	@Test
	@Ignore
	public void testEnableDisable() throws InterruptedException, IOException {
		Thread initializer = new MemorySynchronizer(mpsm, smpsm);
		initializer.start();
		synchronized (initializer) {
			initializer.wait();
		}
		Thread detector = new NetworkInterfaceDetection(mpsm, smpsm);
		detector.start();
		Thread.sleep(10000);
		System.out.println("sudo ifconfig eth0 down");
		// Runtime.getRuntime().exec("sudo ifconfig eth0 down");
		System.out.println("sudo ifconfig wlan0 down");
		// Runtime.getRuntime().exec("sudo ifconfig wlan0 down");
		Thread.sleep(5000);
		// System.out.println("sudo ifconfig eth0 up");
		Runtime.getRuntime().exec("sudo ifconfig eth0 up");
		// System.out.println("sudo ifconfig wlan0 up");
		Runtime.getRuntime().exec("sudo ifconfig wlan0 up");
		Thread.sleep(10000);
		detector.interrupt();
		detector.join();
		initializer.interrupt();
		initializer.join();
		assertEquals("Initializer not dead", false, initializer.isAlive());
	}

}
