package di.uoa.user.activities.fragments.tabs;

import di.uoa.user.R;
import di.uoa.user.db.bases.NodeBase;
import di.uoa.user.tasks.PatternService;
import di.uoa.user.utils.Accumulator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Fragment for devices.
 *
 */
public class NodeTabFragment extends Fragment {
	public final static String BUNDLE_OPTION = "TYPE";
	private NodeClickListener listener;
	private ListView rootView;
	private ArrayAdapter<String> adapter;
	private ServerUpdateReceiver mMessageReceiver;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		mMessageReceiver = new ServerUpdateReceiver();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter(ServerUpdateReceiver.BROADCAST_FILTER));
		rootView = (ListView) inflater.inflate(
				R.layout.fragment_dashboard, container, false);
		listener = new NodeClickListener();
		rootView.setOnItemLongClickListener(listener);
		rootView.setOnItemClickListener(listener);
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_activated_1);
		NodeBase datasource = new NodeBase(getActivity());
		datasource.open();
		for (String node : datasource.getAllNodes()) {
			adapter.add(node);
		}
		datasource.close();
		rootView.setAdapter(adapter);
		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getItemId() == R.id.node_menu) {
			if (listener.lastPosition == -1) {
				Toast.makeText(
						getActivity(),
						Accumulator.getContext().getString(
								R.string.error_invalid_action),
						Toast.LENGTH_SHORT).show();
			} else {
				TextView nodeView = (TextView)rootView.getChildAt(listener.lastPosition);
				NodeBase datasource = new NodeBase(getActivity());
				datasource.open();
				datasource.deleteNode(nodeView.getText().toString());
				datasource.close();
				String node = nodeView.getText().toString();
				Intent mServiceIntent = new Intent(getActivity(), PatternService.class);
				mServiceIntent.putExtra(PatternService.PARAM_DELETE_NODE, node);
				System.out.println(node);
				getActivity().startService(mServiceIntent);
			}
		}
		return super.onOptionsItemSelected(item);
	}


	/**
	 * 
	 * Listener that handles node clicking
	 *
	 */
	private class NodeClickListener implements OnItemLongClickListener, OnItemClickListener{

		public int lastPosition;
		
		public NodeClickListener() {
			lastPosition = -1;
		}
		
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			//This is for the lol
			Toast.makeText(getActivity(), Accumulator.getContext().getString(
					R.string.error_invalid_action), Toast.LENGTH_SHORT)
					.show();
			return false;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			ListView listParent = (ListView) parent;
			//listParent.setItemChecked(position, true);
			view.setBackgroundColor(Color.rgb(255,105,80));
			if(lastPosition != -1) {
				listParent.getChildAt(lastPosition).setBackgroundColor(Color.TRANSPARENT);
			} else if (position == lastPosition) {
				lastPosition = -1;
			}
			lastPosition = position;
		}

		
	}

	public class ServerUpdateReceiver extends BroadcastReceiver {
		 public static final String BROADCAST_FILTER =   
			      "di.uoa.user.SERVER_UPDATED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getStringExtra(PatternService.BROADCAST_ACTION);
			String data = intent.getStringExtra(PatternService.BROADCAST_MESSAGE);
			//Format text and insert it int the view adapter
			if(action.equals(PatternService.PARAM_DELETE_NODE)) {
				adapter.remove(data);
			}
			adapter.notifyDataSetChanged();
		}
	}
}
