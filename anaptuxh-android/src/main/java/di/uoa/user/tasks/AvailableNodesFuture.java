package di.uoa.user.tasks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.type.TypeReference;

import di.uoa.memory.AvailableNodes;
import di.uoa.user.exceptions.AppException;
import di.uoa.user.utils.Account;
import di.uoa.user.utils.ServerConnector;
import android.os.AsyncTask;
import android.util.Log;


/**
 * Task Class to retrieve Available Nodes of Login User from Server and save to Account
 * 
 * @author Steve
 *
 */
public class AvailableNodesFuture extends AsyncTask<Void, Void, AvailableNodes> {

	@Override
	protected AvailableNodes doInBackground(Void... arg0) {
		ServerConnector connector = ServerConnector.getInstance();
		AvailableNodes ret = null;
		List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
		queryParams.add(new BasicNameValuePair("username", Account.getUsername()));
		queryParams.add(new BasicNameValuePair("password", Account.getPassword()));
		try {
			ret = connector.doGET("credentials/android/login", queryParams, new TypeReference<AvailableNodes>(){});
		} catch (AppException e) {
			Log.e(this.getClass().toString(),e.getMessage());
		}
		return ret;
	}

	/**
	 * Refreshes the database of the nodes available.
	 * PS it erases it.
	 */
	@Override
	protected void onPostExecute(AvailableNodes result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Set<String> nodes = new HashSet<String>();
		for(String n : result) {
			nodes.add(n);
		}
		Account.setAvailableNodes(nodes);
	}
}
