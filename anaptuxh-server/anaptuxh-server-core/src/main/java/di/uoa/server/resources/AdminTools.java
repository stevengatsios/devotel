package di.uoa.server.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import di.uoa.server.database.bases.AndroidsBase;
import di.uoa.server.database.bases.MaliciousBase;


@Path("/admin/tools")
public class AdminTools {
	
	/**
	 * Delete nodeId from database
	 * 
	 * @param username
	 * @param password
	 * @param nodeId
	 */
	@POST
	@Path("/remove")
	@Consumes(MediaType.TEXT_PLAIN)
	public void delete(@QueryParam("username") String username,
			@QueryParam("password") String password, String nodeId) {
		if(AndroidsBase.isAdmin(username, password)) {
			AndroidsBase.removeNode(nodeId);
		}	
	}
	
	/**
	 * Send new Malicious Patterns to the the Server
	 * 
	 * @param username
	 * @param password
	 * @param maliciousPatterns
	 */
	@POST
	@Path("/insert")
	@Consumes(MediaType.TEXT_PLAIN)
	public void insertMaliciousPatterns(
			@QueryParam("username") String username,
			String password,
			@QueryParam("pattern") String maliciousPatterns) {
		if(AndroidsBase.isAdmin(username, password)) {
			MaliciousBase.insertMalicious(maliciousPatterns);
		}
	}
	
	/**
	 * Send a request to the Server and retrieve all Malicious Patterns as String
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@GET
	@Path("/malicious")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.TEXT_PLAIN)
	public String retrieveMalicousPatterns(@QueryParam("username") String username,
			@QueryParam("password") String password) {
		String ret = null;
		if(AndroidsBase.isAdmin(username, password)) {
			List<String> malicious = MaliciousBase.getMalicious(true);
			ret = malicious.toString().replaceAll("[\\[\\]]", "").replaceAll(" ", "");
			ret = "\"" + ret + "\""; //To JSONify it
		}
		return ret;
	}

}


