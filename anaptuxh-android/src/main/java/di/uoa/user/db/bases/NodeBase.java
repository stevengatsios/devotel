package di.uoa.user.db.bases;

import java.util.ArrayList;
import java.util.List;

import di.uoa.user.db.MySqliteHelper;
import di.uoa.user.db.tables.Node;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Utility Class to retrieve/update data from/to Nodes Table of database
 * 
 * @author Kostis
 *
 */
public class NodeBase {

	private SQLiteDatabase database;
	private MySqliteHelper dbHelper;
	private String[] allColumns = { MySqliteHelper.COLUMN_ID,
			MySqliteHelper.COLUMN_NODE };

	public NodeBase(Context context) {
		dbHelper = new MySqliteHelper(context);
	}
	
	/**
	 * open database
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	
	/**
	 * close database
	 */
	public void close() {
		dbHelper.close();
	}
	
	/**
	 * Create a new Node {@param node}
	 * 
	 * @param node
	 * @return
	 */
	public Node createNode(String node) {
		ContentValues values = new ContentValues();
		values.put(MySqliteHelper.COLUMN_NODE, node);
		long insertId = database.insert(MySqliteHelper.TABLE_NODES, null,
				values);
		Cursor cursor = database.query(MySqliteHelper.TABLE_NODES, allColumns,
				MySqliteHelper.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		Node newNode = cursorToNode(cursor);
		cursor.close();
		return newNode;
	}
	
	/**
	 * Delete Node {@param node}
	 * 
	 * @param node
	 */
	public void deleteNode(String node) {
		database.delete(MySqliteHelper.TABLE_NODES, MySqliteHelper.COLUMN_NODE
				+ " = '" + node + "'", null);
	}
	
	/**
	 * Delete All Nodes from the Table
	 */
	public void deleteAllNodes() {
		database.delete(MySqliteHelper.TABLE_NODES, null, null);
	}

	/**
	 * Retrieve All Nodes As String
	 * 
	 * @return 
	 */
	public List<String> getAllNodes() {
		List<String> nodes = new ArrayList<String>();
		Cursor cursor = database.query(MySqliteHelper.TABLE_NODES, allColumns,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Node node = cursorToNode(cursor);
			nodes.add(node.getNode());
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return nodes;
	}
	
	/**
	 * Check if Node {@param node} exists in the Table
	 * 
	 * @param node
	 * @return
	 */
	public boolean exists(String node) {
		boolean exists;
		Cursor cursor = database.query(MySqliteHelper.TABLE_NODES, allColumns,
				MySqliteHelper.COLUMN_NODE + " = '" + node + "'", null, null, null, null);
		exists = cursor.moveToFirst();
		cursor.close();
		return exists;
	}
	
	/**
	 * Convert cursor to a Node structure
	 * 
	 * @param cursor
	 * @return
	 */
	private Node cursorToNode(Cursor cursor) {
		Node node = new Node();
		node.setId(cursor.getLong(0));
		node.setNode(cursor.getString(1));
		return node;
	}

}
